
# ------------------------------------------------------------
#  Copyright (c) SILAB , Physics Institute of Bonn University
# ------------------------------------------------------------

#
#   Constraints for the XILINX KC705 evaluation board
#

# Clock domains
# CLK_SYS: 200 MHz, from xtal oscillator
# -> PLL1: CLK40PLL: TDC
#          CLK160PLL: TDC
#          CLK320PLL: TDC
# -> PLL2: BUS_CLK_PLL: 142.86 MHz, main system clock (7 ns)
#          CLK125PLLTX, Ethernet
#          CLK125PLLTX90: Ethernet
# CLK_INIT: 156.25 MHz, from Si570 oscillator
# -> INIT_CLK: Aurora core
# CLK_MGT_REF or CLK_MGT_REF_SMA: 160 MHz, from Si5324 programmable oscilaltor or external clock
# ->        CMDCLK: 160 MHz, command encoder
#           USERCLK: 20 MHz, Aurora word clock
# CLK_GMII_RX: 125 MHz, from Ethernet chip

# Clock inputs
create_clock -period 8.000 -name CLK_GMII_RX -add [get_ports gmii_rx_clk]
create_clock -period 5.000 -name CLK_SYS -add [get_ports CLK200_P]
create_clock -period 6.400 -name CLK_INIT -add [get_ports CLKSi570_P]
create_clock -period 6.250 -name CLK_MGT_REF -add [get_ports Si5324_P]
create_clock -period 6.250 -name CLK_MGT_REF_SMA [get_ports SMA_MGT_REFCLK_P]

# Internally generated clocks (PLL)
create_clock -period 25 [get_pins -hier -filter name=~*generate_aurora_rx[*]*gtxe2_i/TXOUTCLK]
create_clock -period 25 [get_pins -hier -filter name=~*generate_aurora_rx[*]*gtxe2_i/RXOUTCLK]

# Derived clocks (clock dividers)
create_generated_clock -name I2C_CLK -source [get_pins PLLE2_BASE_inst/CLKOUT0] -divide_by 1600 [get_pins i_bdaq53_core/i_clock_divisor_i2c/CLOCK_reg/Q]
create_generated_clock -name SPI_CLK -source [get_pins PLLE2_BASE_inst/CLKOUT0] -divide_by 4 [get_pins i_bdaq53_core/i_clock_divisor_spi/CLOCK_reg/Q]

# Exclude asynchronous clock domains from timing (handled by CDCs)
set_clock_groups -asynchronous \
-group {BUS_CLK_PLL SPI_CLK I2C_CLK} \
-group {CLK125PLLTX CLK125PLLTX90} \
-group {CLK320PLL CLK160PLL CLK40PLL} \
-group [get_clocks -include_generated_clocks CLK_MGT_REF] \
-group [get_clocks -include_generated_clocks CLK_MGT_REF_SMA] \
-group {CLK_INIT} \
-group {CLK_GMII_RX}

# SiTCP
set_max_delay -datapath_only -from [get_clocks CLK_GMII_RX] -to [get_ports gmii_txd*] 3.2
set_max_delay -datapath_only -from [get_clocks CLK_GMII_RX] -to [get_port gmii_tx_en] 3.2
set_max_delay -datapath_only -from [get_clocks CLK_GMII_RX] -to [get_port gmii_tx_er] 3.2
set_max_delay -datapath_only -from [get_clocks CLK_GMII_RX] -to [get_port gmii_tx_clk] 3.2

#set_input_delay -clock [get_clocks CLK_GMII_RX] -min 0.5 [get_port gmii_rxd*]
#set_input_delay -clock [get_clocks CLK_GMII_RX] -min 0.5 [get_port gmii_rx_er]
#set_input_delay -clock [get_clocks CLK_GMII_RX] -min 0.5 [get_port gmii_rx_dv]
#set_input_delay -clock [get_clocks CLK_GMII_RX] -max 5.5 [get_port gmii_rxd*]
#set_input_delay -clock [get_clocks CLK_GMII_RX] -max 5.5 [get_port gmii_rx_er]
#set_input_delay -clock [get_clocks CLK_GMII_RX] -max 5.5 [get_port gmii_rx_dv]

set_property ASYNC_REG true [get_cells { sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_0 sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_1 }]

# false paths (resets etc)
set_false_path -from [get_pins sitcp/SiTCP/BBT_SiTCP_RST/resetReq_1/C]
set_false_path -from [get_pins {i_bdaq53_core/i_aurora_rx/aurora_frame/aurora_64b66b_1lane_block_i/generate_aurora_rx[*].aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/rxresetfsm_i/rx_cdrlocked_reg/C}] -to [get_pins {i_bdaq53_core/i_aurora_rx/aurora_frame/aurora_64b66b_1lane_block_i/generate_aurora_rx[*].aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/rxrecclk_bufg_i/CE0}]
#set_false_path -from [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/pma_init_r_reg/C] -to [get_pins {i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/pma_init_stage_reg[31]_srl32/D}]
set_false_path -to [get_pins {i_bdaq53_core/i_i2c/i_i2c_core/byte_count_reg[*]/CE}]

#Oscillator 200MHz
set_property PACKAGE_PIN AD11 [get_ports CLK200_N]
set_property PACKAGE_PIN AD12 [get_ports CLK200_P]
set_property IOSTANDARD LVDS [get_ports CLK200_*]

#Oscillator Si570 (10...810 MHz, default = 156.25 MHz)
set_property PACKAGE_PIN K29 [get_ports CLKSi570_N]
set_property PACKAGE_PIN K28 [get_ports CLKSi570_P]
set_property IOSTANDARD LVDS_25 [get_ports CLKSi570_*]

#USER SMA CLOCK
#set_property PACKAGE_PIN K25 [get_ports USER_SMA_CLOCK_N]
#set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_N]
#set_property PACKAGE_PIN L25 [get_ports USER_SMA_CLOCK_P]
#set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_P]

#Oscillator  + jitter attenuator (MGT reference clock)
set_property PACKAGE_PIN L7 [get_ports Si5324_N]
set_property PACKAGE_PIN L8 [get_ports Si5324_P]
#set_property PACKAGE_PIN C7 [get_ports Si5342_N]
#set_property PACKAGE_PIN C8 [get_ports Si5342_P]
set_property PACKAGE_PIN AE20 [get_ports SI5324_RST]
set_property IOSTANDARD LVCMOS25 [get_ports SI5324_RST]
set_property PACKAGE_PIN C19 [get_ports SI5342_FMC_RST]
set_property IOSTANDARD LVCMOS25 [get_ports SI5342_FMC_RST]

#Push buttons
set_property PACKAGE_PIN AB7 [get_ports RESET_BUTTON]
set_property IOSTANDARD LVCMOS15 [get_ports RESET_BUTTON]
set_property PACKAGE_PIN G12 [get_ports GPIO_SW_C]
set_property IOSTANDARD LVCMOS25 [get_ports GPIO_SW_C]
#set_property PACKAGE_PIN AG5 [get_ports GPIO_SW_E]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_E]
#set_property PACKAGE_PIN AA12 [get_ports GPIO_SW_N]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_N]
#set_property PACKAGE_PIN AB12 [get_ports GPIO_SW_S]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_S]
#set_property PACKAGE_PIN AC6 [get_ports GPIO_SW_W]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_W]


#SITCP
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN R23 [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN J21 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS25 [get_ports phy_rst_n]
set_property PACKAGE_PIN L20 [get_ports phy_rst_n]


# GMII interface (KC705)
set_property PACKAGE_PIN R30 [get_ports gmii_crs]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_crs]
set_property PACKAGE_PIN W19 [get_ports gmii_col]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_col]

set_property PACKAGE_PIN U27 [get_ports gmii_rx_clk]
set_property PACKAGE_PIN R28 [get_ports gmii_rx_dv]
set_property PACKAGE_PIN V26 [get_ports gmii_rx_er]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_rx_*]
set_property PACKAGE_PIN U30 [get_ports {gmii_rxd[0]}]
set_property PACKAGE_PIN U25 [get_ports {gmii_rxd[1]}]
set_property PACKAGE_PIN T25 [get_ports {gmii_rxd[2]}]
set_property PACKAGE_PIN U28 [get_ports {gmii_rxd[3]}]
set_property PACKAGE_PIN R19 [get_ports {gmii_rxd[4]}]
set_property PACKAGE_PIN T27 [get_ports {gmii_rxd[5]}]
set_property PACKAGE_PIN T26 [get_ports {gmii_rxd[6]}]
set_property PACKAGE_PIN T28 [get_ports {gmii_rxd[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {gmii_rxd[*]}]

set_property PACKAGE_PIN K30 [get_ports gmii_tx_clk]
set_property PACKAGE_PIN M27 [get_ports gmii_tx_en]
set_property PACKAGE_PIN N29 [get_ports gmii_tx_er]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_tx_*]
set_property SLEW FAST [get_ports gmii_tx_*]
set_property PACKAGE_PIN N27 [get_ports {gmii_txd[0]}]
set_property PACKAGE_PIN N25 [get_ports {gmii_txd[1]}]
set_property PACKAGE_PIN M29 [get_ports {gmii_txd[2]}]
set_property PACKAGE_PIN L28 [get_ports {gmii_txd[3]}]
set_property PACKAGE_PIN J26 [get_ports {gmii_txd[4]}]
set_property PACKAGE_PIN K26 [get_ports {gmii_txd[5]}]
set_property PACKAGE_PIN L30 [get_ports {gmii_txd[6]}]
set_property PACKAGE_PIN J28 [get_ports {gmii_txd[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {gmii_txd[*]}]
set_property SLEW FAST [get_ports {gmii_txd[*]}]


# Aurora related signals: KC705 features 16 MGT pairs.
# SMA: MGT_BANK_117, GTXE2_CHANNEL_X0Y8

#set_property PACKAGE_PIN L7 [get_ports MGT_REFCLK0_N]
#set_property PACKAGE_PIN L8 [get_ports MGT_REFCLK0_P]

set_property PACKAGE_PIN J7 [get_ports SMA_MGT_REFCLK_N]
set_property PACKAGE_PIN J8 [get_ports SMA_MGT_REFCLK_P]

set_property PACKAGE_PIN K6 [get_ports {MGT_RX_P[0]}]
set_property PACKAGE_PIN K5 [get_ports {MGT_RX_N[0]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_P[1]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_N[1]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_P[2]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_N[2]}]
#set_property PACKAGE_PIN K6 [get_ports {MGT_RX_P[3]}]
#set_property PACKAGE_PIN K5 [get_ports {MGT_RX_N[3]}]

set_property PACKAGE_PIN F6 [get_ports {MGT_RX_FMC_LPC_P[0]}]
set_property PACKAGE_PIN F5 [get_ports {MGT_RX_FMC_LPC_N[0]}]
#set_property PACKAGE_PIN E4 [get_ports {MGT_RX_FMC_HPC_P[0]}]
#set_property PACKAGE_PIN E3 [get_ports {MGT_RX_FMC_HPC_N[0]}]

#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK0_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK0_P]
#set_property PACKAGE_PIN  [get_ports MGT_REFCLK1_P]
#set_property PACKAGE_PIN  [get_ports MGT_REFCLK1_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK1_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK1_P]


#set_property PACKAGE_PIN K2 [get_ports {MGT_TX_P[0]}]
#set_property PACKAGE_PIN K1 [get_ports {MGT_TX_N[0]}]
set_property PACKAGE_PIN F2 [get_ports {MGT_TX_FMC_LPC_P[0]}]
set_property PACKAGE_PIN F1 [get_ports {MGT_TX_FMC_LPC_N[0]}]
#set_property PACKAGE_PIN D2 [get_ports {MGT_TX_FMC_HPC_P[0]}]
#set_property PACKAGE_PIN D1 [get_ports {MGT_TX_FMC_HPC_N[0]}]

#set_property IOSTANDARD LVDS [get_ports RX_INIT_CLK_*]
#set_property DIFF_TERM false [get_ports RX_INIT_CLK_*]
#set_property PACKAGE_PIN  [get_ports RX_INIT_CLK_P]
#set_property PACKAGE_PIN  [get_ports RX_INIT_CLK_N]


# Debug LEDs
set_property PACKAGE_PIN AB8 [get_ports {LED[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[0]}]
set_property PACKAGE_PIN AA8 [get_ports {LED[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[1]}]
set_property PACKAGE_PIN AC9 [get_ports {LED[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[2]}]
set_property PACKAGE_PIN AB9 [get_ports {LED[3]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[3]}]
set_property PACKAGE_PIN AE26 [get_ports {LED[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[4]}]
set_property PACKAGE_PIN G19 [get_ports {LED[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[5]}]
set_property PACKAGE_PIN E18 [get_ports {LED[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[6]}]
set_property PACKAGE_PIN F16 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[7]}]
set_property SLEW SLOW [get_ports LED*]

#set_property PACKAGE_PIN F20 [get_ports LEMO_TX0]
set_property PACKAGE_PIN AD28 [get_ports LEMO_TX1]
set_property IOSTANDARD LVCMOS25 [get_ports LEMO_TX*]
set_property SLEW FAST [get_ports LEMO_TX*]

#set_property PACKAGE_PIN AD28 [get_ports LEMO_BUSY_LPC]
#set_property PACKAGE_PIN E21 [get_ports LEMO_BUSY_HPC]
#set_property IOSTANDARD LVCMOS25 [get_ports LEMO_BUSY*]
#set_property SLEW FAST [get_ports LEMO_BUSY*]
# TLU

#set_property PACKAGE_PIN F21 [get_ports LEMO_TRIGGER_HPC]
set_property PACKAGE_PIN AD27 [get_ports RJ45_TRIGGER]
set_property IOSTANDARD LVCMOS25 [get_ports RJ45_TRIGGER]
#set_property PACKAGE_PIN E20 [get_ports RJ45_RESET]
#set_property IOSTANDARD LVCMOS25 [get_ports RJ45_RESET]

# DP_ML ("DP2") connected to SelectIOs
set_property PACKAGE_PIN AF20 [get_ports DP_GPIO_LANE0_LPC_P]
set_property PACKAGE_PIN AF21 [get_ports DP_GPIO_LANE0_LPC_N]
set_property PACKAGE_PIN AH21 [get_ports DP_GPIO_LANE1_LPC_P]
set_property PACKAGE_PIN AJ21 [get_ports DP_GPIO_LANE1_LPC_N]
set_property PACKAGE_PIN AG25 [get_ports DP_GPIO_LANE2_LPC_P]
set_property PACKAGE_PIN AH25 [get_ports DP_GPIO_LANE2_LPC_N]
set_property PACKAGE_PIN AE25 [get_ports DP_GPIO_LANE3_LPC_P]
set_property PACKAGE_PIN AF25 [get_ports DP_GPIO_LANE3_LPC_N]
#set_property PACKAGE_PIN H24 [get_ports DP_GPIO_LANE0_HPC_P]
#set_property PACKAGE_PIN H25 [get_ports DP_GPIO_LANE0_HPC_N]
#set_property PACKAGE_PIN G28 [get_ports DP_GPIO_LANE1_HPC_P]
#set_property PACKAGE_PIN F28 [get_ports DP_GPIO_LANE1_HPC_N]
#set_property PACKAGE_PIN E28 [get_ports DP_GPIO_LANE2_HPC_P]
#set_property PACKAGE_PIN D28 [get_ports DP_GPIO_LANE2_HPC_N]
#set_property PACKAGE_PIN G27 [get_ports DP_GPIO_LANE3_HPC_P]
#set_property PACKAGE_PIN F27 [get_ports DP_GPIO_LANE3_HPC_N]
set_property IOSTANDARD LVDS_25 [get_ports DP_GPIO_LANE*]

# User SMA: CMD encoder. Bank ...
set_property PACKAGE_PIN Y23 [get_ports USER_SMA_P]
set_property PACKAGE_PIN Y24 [get_ports USER_SMA_N]
set_property IOSTANDARD LVDS_25 [get_ports USER_SMA*]
#set_property IOSTANDARD LVCMOS25 [get_ports USER_SMA*]
#set_property PACKAGE_PIN AD23 [get_ports CMD_FMC_P]
#set_property PACKAGE_PIN AE24 [get_ports CMD_FMC_N]
#set_property IOSTANDARD LVDS_25 [get_ports CMD_FMC*]
#set_property PACKAGE_PIN C25 [get_ports CMD_FMC_HPC_P]
#set_property PACKAGE_PIN B25 [get_ports CMD_FMC_HPC_N]
set_property PACKAGE_PIN AD23 [get_ports CMD_FMC_LPC_P]
set_property PACKAGE_PIN AE24 [get_ports CMD_FMC_LPC_N]
set_property IOSTANDARD LVDS_25 [get_ports CMD_FMC*]


# Bypass mode clocks
set_property IOSTANDARD LVDS_25 [get_ports DP1_EXT_CMD_CLK*]
set_property PACKAGE_PIN AA20 [get_ports DP1_EXT_CMD_CLK_P]
set_property PACKAGE_PIN AB20 [get_ports DP1_EXT_CMD_CLK_N]
set_property IOSTANDARD LVDS_25 [get_ports DP1_EXT_SER_CLK*]
set_property PACKAGE_PIN AG29 [get_ports DP1_EXT_SER_CLK_P]
set_property PACKAGE_PIN AH29 [get_ports DP1_EXT_SER_CLK_N]


# DP1_EN
set_property PACKAGE_PIN AF27 [get_ports DP1_EN_LPC]
set_property IOSTANDARD LVCMOS25 [get_ports DP1_EN*]


# DP2_EN
set_property PACKAGE_PIN  AD24  [get_ports DP2_EN_LPC]
#set_property PACKAGE_PIN  B24  [get_ports DP2_EN_HPC]
set_property IOSTANDARD LVCMOS25 [get_ports DP2_EN*]


# DIP switch
set_property PACKAGE_PIN Y28 [get_ports {GPIO_DIP_SW[3]}]
set_property PACKAGE_PIN AA28 [get_ports {GPIO_DIP_SW[2]}]
set_property PACKAGE_PIN W29 [get_ports {GPIO_DIP_SW[1]}]
set_property PACKAGE_PIN Y29 [get_ports {GPIO_DIP_SW[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports GPIO_DIP_SW*]


# I2C pins
# Bus switch TI PCA9548 at address 0x74
# Switch address 0: Si570 clock
set_property PACKAGE_PIN K21 [get_ports I2C_SCL]
set_property PACKAGE_PIN L21 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS25 [get_ports I2C_*]
set_property SLEW SLOW [get_ports I2C_*]


# Fan
set_property PACKAGE_PIN L26 [get_ports SM_FAN_PWM]
set_property IOSTANDARD LVCMOS25 [get_ports SM_FAN_PWM]
#set_property PACKAGE_PIN U22 [get_ports SM_FAN_TACH]
#set_property IOSTANDARD LVCMOS25 [get_ports SM_FAN_TACH]


#XADC
#set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_01_P]
#set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_01_N]
#set_property PACKAGE_PIN AA25 [get_ports XADC_GPIO_01_P]
#set_property PACKAGE_PIN AB25 [get_ports XADC_GPIO_01_N]
set_property PACKAGE_PIN AA27 [get_ports XADC_GPIO_23_P]
set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_23_P]
set_property PACKAGE_PIN AB28 [get_ports XADC_GPIO_23_N]
set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_23_N]
#in case we want to use the gpios as single ended signals
set_property IOSTANDARD LVCMOS25 [get_ports XADC_GPIO_23_P]
set_property IOSTANDARD LVCMOS25 [get_ports XADC_GPIO_23_N]


# Aurora IP core
#set_property LOC GTXE2_CHANNEL_X0Y8 [get_cells aurora_64b66b_1lane_block_i/aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/aurora_64b66b_1lane_multi_gt_i/aurora_64b66b_1lane_gtx_inst/gtxe2_i]
set_false_path -to [get_pins -hier *aurora_64b66b_1lane_cdc_to*/D]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *data_sync_reg1}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *ack_sync_reg1}]

# Boot memory
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
