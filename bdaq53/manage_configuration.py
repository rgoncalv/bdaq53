#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
This module handles configuration files. Fresh configuration files can be generated from database data.
YARR configuration files can be converted to BDAQ configuration files and vice versa.
'''

import sys
import os
import argparse
import yaml
import json
import tables as tb
import numpy as np
import requests

# FIXME: Find a better solution for import of internal modules so this script can be called via CLI from both outside and within the package
try:
    from bdaq53.system import logger
    from bdaq53 import manage_databases
except ImportError:
    import logger
    import manage_databases


log = logger.setup_derived_logger('Config Manager')
root_dir = os.path.dirname(os.path.abspath(__file__))
bdaq_default_config_path = os.path.join(root_dir, os.path.join('chips', 'rd53a_default.cfg.yaml'))
yarr_default_config_url = 'https://gitlab.cern.ch/YARR/YARR/raw/master/configs/defaults/default_rd53a.json'

n_cols, n_rows = 400, 192


#
# Hardcoded data
#

# Mapping for register names
trim_register_map = {   # Trimbits
    'VREF_A_TRIM': 'SldoAnalogTrim',
    'VREF_D_TRIM': 'SldoDigitalTrim',
    'MON_BG_TRIM': 'AdcRefTrim',
    'MON_ADC_TRIM': 'AdcTrim'
}

afe_register_map = {    # Analog FE registers
    'IBIASP1_SYNC': 'SyncIbiasp1',
    'IBIASP2_SYNC': 'SyncIbiasp2',
    'IBIAS_SF_SYNC': 'SyncIbiasSf',
    'IBIAS_KRUM_SYNC': 'SyncIbiasKrum',
    'IBIAS_DISC_SYNC': 'SyncIbiasDisc',
    'ICTRL_SYNCT_SYNC': 'SyncIctrlSynct',
    'CONF_FE_SYNC': {'size': 5, 'SyncFastTot': (0, 1), 'SyncSelC4F': (1, 2), 'SyncSelC2F': (2, 3), 'SyncAutoZero': (3, 5)},
    'VBL_SYNC': 'SyncVbl',
    'VTH_SYNC': 'SyncVth',
    'VREF_KRUM_SYNC': 'SyncVrefKrum',
    'PA_IN_BIAS_LIN': 'LinPaInBias',
    'FC_BIAS_LIN': 'LinFcBias',
    'KRUM_CURR_LIN': 'LinKrumCurr',
    'LDAC_LIN': 'LinLdac',
    'COMP_LIN': 'LinComp',
    'REF_KRUM_LIN': 'LinRefKrum',
    'Vthreshold_LIN': 'LinVth',
    'PRMP_DIFF': 'DiffPrmp',
    'FOL_DIFF': 'DiffFol',
    'PRECOMP_DIFF': 'DiffPrecomp',
    'COMP_DIFF': 'DiffComp',
    'VFF_DIFF': 'DiffVff',
    'VTH1_DIFF': 'DiffVth1',
    'VTH2_DIFF': 'DiffVth2',
    'LCC_DIFF': 'DiffLcc',
    'CONF_FE_DIFF': {'size': 2, 'DiffFbCapEn': (0, 1), 'DiffLccEn': (1, 2)}
}

global_register_map = {  # Global registers
    'PIX_PORTAL': 'PixPortal',
    'REGION_COL': 'PixRegionCol',
    'REGION_ROW': 'PixRegionRow',
    'PIX_MODE': {'size': 6, 'PixBroadcastMask': (0, 3), 'PixAutoRow': (3, 4), 'PixAutoCol': (4, 5), 'PixBroadcastEn': (5, 6)},
    'PIX_DEFAULT_CONFIG': 'PixDefaultConfig',
    'EN_CORE_COL_SYNC': 'EnCoreColSync',
    'EN_CORE_COL_LIN_1': 'EnCoreColLin1',
    'EN_CORE_COL_LIN_2': 'EnCoreColLin2',
    'EN_CORE_COL_DIFF_1': 'EnCoreColDiff1',
    'EN_CORE_COL_DIFF_2': 'EnCoreColDiff2',
    'LATENCY_CONFIG': 'LatencyConfig',
    'WR_SYNC_DELAY_SYNC': 'WrSyncDelaySync',
    'INJECTION_SELECT': {'size': 6, 'InjDelay': (0, 4), 'InjEnDig': (4, 5), 'InjAnaMode': (5, 6)},
    'CLK_DATA_DELAY': {'size': 9, 'CmdDelay': (0, 4), 'ClkDelay': (4, 8), 'ClkDelaySel': (8, 9)},
    'VCAL_HIGH': 'InjVcalHigh',
    'VCAL_MED': 'InjVcalMed',
    'CH_SYNC_CONF': {'size': 12, 'ChSyncUnlock': (0, 5), 'ChSyncLock': (5, 10), 'ChSyncPhase': (10, 12)},
    'GLOBAL_PULSE_ROUTE': 'GlobalPulseRt',
    'MONITOR_FRAME_SKIP': 'MonFrameSkip',
    'EN_MACRO_COL_CAL_SYNC_1': 'CalColprSync1',
    'EN_MACRO_COL_CAL_SYNC_2': 'CalColprSync2',
    'EN_MACRO_COL_CAL_SYNC_3': 'CalColprSync3',
    'EN_MACRO_COL_CAL_SYNC_4': 'CalColprSync4',
    'EN_MACRO_COL_CAL_LIN_1': 'CalColprLin1',
    'EN_MACRO_COL_CAL_LIN_2': 'CalColprLin2',
    'EN_MACRO_COL_CAL_LIN_3': 'CalColprLin3',
    'EN_MACRO_COL_CAL_LIN_4': 'CalColprLin4',
    'EN_MACRO_COL_CAL_LIN_5': 'CalColprLin5',
    'EN_MACRO_COL_CAL_DIFF_1': 'CalColprDiff1',
    'EN_MACRO_COL_CAL_DIFF_2': 'CalColprDiff2',
    'EN_MACRO_COL_CAL_DIFF_3': 'CalColprDiff3',
    'EN_MACRO_COL_CAL_DIFF_4': 'CalColprDiff4',
    'EN_MACRO_COL_CAL_DIFF_5': 'CalColprDiff5',
    'DEBUG_CONFIG': 'DebugConfig',
    'OUTPUT_CONFIG': {'size': 9, 'OutputFmt': (0, 2), 'OutputActiveLanes': (2, 6), 'OutputSerType': (6, 7), 'OutputDataReadDelay': (7, 9)},
    'OUT_PAD_CONFIG': 'OutPadConfig',
    'GP_LVDS_ROUTE': 'GpLvdsRoute',
    'CDR_CONFIG': {'size': 14, 'CdrSelSerClk': (0, 3), 'CdrVcoGain': (3, 6), 'CdrEnGck': (6, 7), 'CdrPdDel': (7, 11), 'CdrPdSel': (11, 13), 'CdrSelDelClk': (13, 14)},
    'CDR_VCO_BUFF_BIAS': 'VcoBuffBias',
    'CDR_CP_IBIAS': 'CdrCpIbias',
    'CDR_VCO_IBIAS': 'VcoIbias',
    'SER_SEL_OUT': {'size': 8, 'SerSelOut0': (0, 2), 'SerSelOut1': (2, 4), 'SerSelOut2': (4, 6), 'SerSelOut3': (6, 8)},
    'CML_CONFIG': {'size': 8, 'CmlEn': (0, 4), 'CmlEnTap': (4, 6), 'CmlInvTap': (6, 8)},
    'CML_TAP0_BIAS': 'CmlTapBias0',
    'CML_TAP1_BIAS': 'CmlTapBias1',
    'CML_TAP2_BIAS': 'CmlTapBias2',
    'AURORA_CC_CONFIG': {'size': 8, 'AuroraCcSend': (0, 2), 'AuroraCcWait': (2, 8)},
    'AURORA_CB_CONFIG0': {'size': 8, 'AuroraCbSend': (0, 4), 'AuroraCbWaitLow': (4, 8)},
    'AURORA_CB_CONFIG1': 'AuroraCbWaitHigh',
    'AURORA_INIT_WAIT': 'AuroraInitWait',
    'MONITOR_SELECT': {'size': 14, 'MonitorVmonMux': (0, 7), 'MonitorImonMux': (7, 13), 'MonitorEnable': (13, 14)},
    'HITOR_0_MASK_SYNC': 'HitOr0MaskSync',
    'HITOR_1_MASK_SYNC': 'HitOr1MaskSync',
    'HITOR_2_MASK_SYNC': 'HitOr2MaskSync',
    'HITOR_3_MASK_SYNC': 'HitOr3MaskSync',
    'HITOR_0_MASK_LIN_0': 'HitOr0MaskLin0',
    'HITOR_0_MASK_LIN_1': 'HitOr0MaskLin1',
    'HITOR_1_MASK_LIN_0': 'HitOr1MaskLin0',
    'HITOR_1_MASK_LIN_1': 'HitOr1MaskLin1',
    'HITOR_2_MASK_LIN_0': 'HitOr2MaskLin0',
    'HITOR_2_MASK_LIN_1': 'HitOr2MaskLin1',
    'HITOR_3_MASK_LIN_0': 'HitOr3MaskLin0',
    'HITOR_3_MASK_LIN_1': 'HitOr3MaskLin1',
    'HITOR_0_MASK_DIFF_0': 'HitOr0MaskDiff0',
    'HITOR_0_MASK_DIFF_1': 'HitOr0MaskDiff1',
    'HITOR_1_MASK_DIFF_0': 'HitOr1MaskDiff0',
    'HITOR_1_MASK_DIFF_1': 'HitOr1MaskDiff1',
    'HITOR_2_MASK_DIFF_0': 'HitOr2MaskDiff0',
    'HITOR_2_MASK_DIFF_1': 'HitOr2MaskDiff1',
    'HITOR_3_MASK_DIFF_0': 'HitOr3MaskDiff0',
    'HITOR_3_MASK_DIFF_1': 'HitOr3MaskDiff1',
    'MONITOR_CONFIG': 'AdcTrim',
    'SENSOR_CONFIG_0': 'SensorCfg0',
    'SENSOR_CONFIG_1': 'SensorCfg1',
    'AutoRead0': 'AutoReadA0',
    'AutoRead1': 'AutoReadB0',
    'AutoRead2': 'AutoReadA1',
    'AutoRead3': 'AutoReadB1',
    'AutoRead4': 'AutoReadA2',
    'AutoRead5': 'AutoReadB2',
    'AutoRead6': 'AutoReadA3',
    'AutoRead7': 'AutoReadB3',
    'RING_OSC_ENABLE': 'RingOscEn',
    'RING_OSC_0': 'RingOsc0',
    'RING_OSC_1': 'RingOsc1',
    'RING_OSC_2': 'RingOsc2',
    'RING_OSC_3': 'RingOsc3',
    'RING_OSC_4': 'RingOsc4',
    'RING_OSC_5': 'RingOsc5',
    'RING_OSC_6': 'RingOsc6',
    'RING_OSC_7': 'RingOsc7',
    'BCIDCnt': 'BcCounter',
    'TrigCnt': 'TrigCounter',
    'LockLossCnt': 'LockLossCounter',
    'BitFlipWngCnt': 'BflipWarnCounter',
    'BitFlipErrCnt': 'BflipErrCounter',
    'CmdErrCnt': 'CmdErrCounter',
    'WngFifoFullCnt_0': 'FifoFullCounter0',
    'WngFifoFullCnt_1': 'FifoFullCounter1',
    'WngFifoFullCnt_2': 'FifoFullCounter2',
    'WngFifoFullCnt_3': 'FifoFullCounter3',
    'AI_REGION_COL': 'AiPixCol',
    'AI_REGION_ROW': 'AiPixRow',
    'HitOr_0_Cnt': 'HitOrCounter0',
    'HitOr_1_Cnt': 'HitOrCounter1',
    'HitOr_2_Cnt': 'HitOrCounter2',
    'HitOr_3_Cnt': 'HitOrCounter3',
    'SkippedTriggerCnt': 'SkipTriggerCounter',
    'ErrWngMask': 'ErrMask',
    'MonitoringDataADC': 'AdcRead',
}


# Default YARR config in case there is no internet access.
# Corresponds to rd53a_default.json from master branch, downloaded 2019-11-14.
yarr_fallback_config = {
    "GlobalConfig": {
        "AdcRead": 0,
        "AdcRefTrim": 12,
        "AdcTrim": 5,
        "AiPixCol": 0,
        "AiPixRow": 0,
        "AuroraCbSend": 0,
        "AuroraCbWaitHigh": 15,
        "AuroraCbWaitLow": 15,
        "AuroraCcSend": 3,
        "AuroraCcWait": 25,
        "AuroraInitWait": 32,
        "AutoReadA0": 136,
        "AutoReadA1": 118,
        "AutoReadA2": 120,
        "AutoReadA3": 122,
        "AutoReadB0": 130,
        "AutoReadB1": 119,
        "AutoReadB2": 121,
        "AutoReadB3": 123,
        "BcCounter": 0,
        "BflipErrCounter": 0,
        "BflipWarnCounter": 0,
        "CalColprDiff1": 65535,
        "CalColprDiff2": 65535,
        "CalColprDiff3": 65535,
        "CalColprDiff4": 65535,
        "CalColprDiff5": 15,
        "CalColprLin1": 65535,
        "CalColprLin2": 65535,
        "CalColprLin3": 65535,
        "CalColprLin4": 65535,
        "CalColprLin5": 15,
        "CalColprSync1": 65535,
        "CalColprSync2": 65535,
        "CalColprSync3": 65535,
        "CalColprSync4": 65535,
        "CdrCpIbias": 50,
        "CdrEnGck": 0,
        "CdrPdDel": 8,
        "CdrPdSel": 0,
        "CdrSelDelClk": 0,
        "CdrSelSerClk": 3,
        "CdrVcoGain": 3,
        "ChSyncLock": 16,
        "ChSyncPhase": 0,
        "ChSyncUnlock": 8,
        "ClkDelay": 0,
        "ClkDelaySel": 0,
        "CmdDelay": 0,
        "CmdErrCounter": 0,
        "CmlEn": 15,
        "CmlEnTap": 1,
        "CmlInvTap": 0,
        "CmlTapBias0": 600,
        "CmlTapBias1": 0,
        "CmlTapBias2": 0,
        "DebugConfig": 0,
        "DiffComp": 1000,
        "DiffFbCapEn": 0,
        "DiffFol": 500,
        "DiffLcc": 20,
        "DiffLccEn": 0,
        "DiffPrecomp": 400,
        "DiffPrmp": 500,
        "DiffVff": 50,
        "DiffVth1": 250,
        "DiffVth2": 50,
        "EnCoreColDiff1": 65535,
        "EnCoreColDiff2": 1,
        "EnCoreColLin1": 65535,
        "EnCoreColLin2": 1,
        "EnCoreColSync": 65535,
        "ErrMask": 0,
        "FifoFullCounter0": 0,
        "FifoFullCounter1": 0,
        "FifoFullCounter2": 0,
        "FifoFullCounter3": 0,
        "GlobalPulseRt": 16640,
        "GpLvdsRoute": 0,
        "HitOr0MaskDiff0": 0,
        "HitOr0MaskDiff1": 0,
        "HitOr0MaskLin0": 0,
        "HitOr0MaskLin1": 0,
        "HitOr0MaskSync": 0,
        "HitOr1MaskDiff0": 0,
        "HitOr1MaskDiff1": 0,
        "HitOr1MaskLin0": 0,
        "HitOr1MaskLin1": 0,
        "HitOr1MaskSync": 0,
        "HitOr2MaskDiff0": 0,
        "HitOr2MaskDiff1": 0,
        "HitOr2MaskLin0": 0,
        "HitOr2MaskLin1": 0,
        "HitOr2MaskSync": 0,
        "HitOr3MaskDiff0": 0,
        "HitOr3MaskDiff1": 0,
        "HitOr3MaskLin0": 0,
        "HitOr3MaskLin1": 0,
        "HitOr3MaskSync": 0,
        "HitOrCounter0": 0,
        "HitOrCounter1": 0,
        "HitOrCounter2": 0,
        "HitOrCounter3": 0,
        "InjAnaMode": 0,
        "InjDelay": 0,
        "InjEnDig": 0,
        "InjVcalDiff": 0,
        "InjVcalHigh": 1000,
        "InjVcalMed": 1000,
        "LatencyConfig": 64,
        "LinComp": 110,
        "LinFcBias": 20,
        "LinKrumCurr": 50,
        "LinLdac": 100,
        "LinPaInBias": 300,
        "LinRefKrum": 300,
        "LinVth": 400,
        "LockLossCounter": 0,
        "MonFrameSkip": 200,
        "MonitorEnable": 0,
        "MonitorImonMux": 63,
        "MonitorVmonMux": 127,
        "OutPadConfig": 5124,
        "OutputActiveLanes": 15,
        "OutputDataReadDelay": 0,
        "OutputFmt": 0,
        "OutputSerType": 0,
        "PixAutoCol": 0,
        "PixAutoRow": 0,
        "PixBroadcastEn": 0,
        "PixBroadcastMask": 0,
        "PixDefaultConfig": 0,
        "PixPortal": 1796,
        "PixRegionCol": 199,
        "PixRegionRow": 135,
        "RingOsc0": 0,
        "RingOsc1": 0,
        "RingOsc2": 0,
        "RingOsc3": 0,
        "RingOsc4": 0,
        "RingOsc5": 0,
        "RingOsc6": 0,
        "RingOsc7": 0,
        "RingOscEn": 0,
        "SelfTrigEn": 0,
        "SensorCfg0": 0,
        "SensorCfg1": 0,
        "SerSelOut0": 1,
        "SerSelOut1": 1,
        "SerSelOut2": 1,
        "SerSelOut3": 1,
        "SkipTriggerCounter": 0,
        "SldoAnalogTrim": 22,
        "SldoDigitalTrim": 22,
        "SyncAutoZero": 0,
        "SyncFastTot": 0,
        "SyncIbiasDisc": 300,
        "SyncIbiasKrum": 55,
        "SyncIbiasSf": 80,
        "SyncIbiasp1": 80,
        "SyncIbiasp2": 120,
        "SyncIctrlSynct": 100,
        "SyncSelC2F": 0,
        "SyncSelC4F": 1,
        "SyncVbl": 400,
        "SyncVrefKrum": 450,
        "SyncVth": 300,
        "TrigCounter": 0,
        "VcoBuffBias": 400,
        "VcoIbias": 500,
        "WrSyncDelaySync": 16
    },
    "Parameter": {
        "ChipId": 0,
        "InjCap": 8.2,
        "Name": "JohnDoe_0",
        "VcalPar": [-1.0, 0.195, 0.0, 0.0]
    },
    "PixelConfig": []
}


#
# General functions
#


def generate_mapping_dicts(bdaq_register_file, yarr_register_file):
    '''
        Automatically generates lookup dictionaries for register names.
        To be copied in this file manually.

        Parameters
        ----------
        bdaq_register_file : str
            YAML file containing a list of all BDAQ registers with addresses.

        yarr_register_file : str
            YAML file containing a list of all YARR registers with addresses.

        Returns
        -------
        global_register_lookup : dict
            Lookup dictionary for global registers
        trim_register_lookup : dict
            Lookup dictionary for trimbit registers
        afe_register_lookup : dict
            Lookup dictionary for AFE registers
    '''

    # Known and handled exceptions are:
    # Address   Name                Exception
    #
    # 3         PIX_MODE            YARR has configuration bits separated
    # 29        CONF_FE_DIFF        YARR has configuration bits separated
    # 30        CONF_FE_SYNC        YARR has configuration bits separated
    # 31        VOLTAGE_TRIM        Both have Analog and Digital trimbits separated
    # 39        INJECTION_SELECT    YARR has configuration bits separated
    # 40        CLK_DATA_DELAY      YARR has configuration bits separated
    # 43        CH_SYNC_CONF        YARR has configuration bits separated
    # 61        OUTPUT_CONFIG       YARR has configuration bits separated
    # 64        CDR_CONFIG          YARR has configuration bits separated
    # 68        SER_SEL_OUT         YARR has configuration bits separated
    # 69        CML_CONFIG          YARR has configuration bits separated
    # 73        AURORA_CC_CONFIG    YARR has configuration bits separated
    # 74        AURORA_CB_CONFIG0   YARR has configuration bits separated
    # 77        MONITOR_SELECT      YARR has configuration bits separated
    # 98        MONITOR_CONFIG      Both have VREF and ADC trimbits separated

    known_exceptions = [3, 29, 30, 31, 39, 40, 43, 61, 64, 68, 69, 73, 74, 77, 98]

    with open(bdaq_register_file, 'r') as f:
        bdaq_registers = {}
        for reg in yaml.safe_load(f)['registers']:
            address = int(reg['address'], 16)
            if address in bdaq_registers.keys() and address not in known_exceptions:
                log.warning('Address {0} is used more than once in BDAQ configuration!'.format(address))
            else:
                bdaq_registers[address] = reg['name']
    with open(yarr_register_file, 'r') as f:
        yarr_registers = {}
        for reg in yaml.safe_load(f)['registers']:
            address = int(reg['address'])
            if address in yarr_registers.keys() and address not in known_exceptions:
                log.warning('Address {0} is used more than once in YARR configuration!'.format(address))
            else:
                yarr_registers[address] = reg['name']

    global_register_lookup, trim_register_lookup, afe_register_lookup = {}, {}, {}

    for address in range(5):    # Some global registers
        try:
            global_register_lookup[bdaq_registers[address]] = yarr_registers[address]
        except KeyError:
            pass
    for address in range(5, 29):    # AFE registers have addresses 5 - 30
        try:
            afe_register_lookup[bdaq_registers[address]] = yarr_registers[address]
        except KeyError:
            pass
    for address in range(32, 137):  # More global registers, SELF_TRIGGER_ENABLE doesn't exist
        try:
            global_register_lookup[bdaq_registers[address]] = yarr_registers[address]
        except KeyError:
            pass

    # Handle exceptions
    trim_register_lookup['VREF_A_TRIM'] = 'SldoAnalogTrim'
    trim_register_lookup['VREF_D_TRIM'] = 'SldoDigitalTrim'
    trim_register_lookup['MON_BG_TRIM'] = 'AdcRefTrim'
    trim_register_lookup['MON_ADC_TRIM'] = 'AdcTrim'

    afe_register_lookup['CONF_FE_SYNC'] = {'size': 5, 'SyncAutoZero': (0, 2), 'SyncSelC2F': (2, 3), 'SyncSelC4F': (3, 4), 'SyncFastTot': (4, 5)}
    afe_register_lookup['CONF_FE_DIFF'] = {'size': 2, 'DiffLccEn': (0, 1), 'DiffFbCapEn': (1, 2)}

    global_register_lookup['PIX_MODE'] = {'size': 6, 'PixBroadcastMask': (0, 3), 'PixAutoRow': (3, 4), 'PixAutoCol': (4, 5), 'PixBroadcastEn': (5, 6)}
    global_register_lookup['INJECTION_SELECT'] = {'size': 6, 'InjDelay': (0, 4), 'InjEnDig': (4, 5), 'InjAnaMode': (5, 6)}
    global_register_lookup['CLK_DATA_DELAY'] = {'size': 9, 'CmdDelay': (0, 4), 'ClkDelay': (4, 8), 'ClkDelaySel': (8, 9)}
    global_register_lookup['CH_SYNC_CONF'] = {'size': 12, 'ChSyncUnlock': (0, 5), 'ChSyncLock': (5, 10), 'ChSyncPhase': (10, 12)}
    global_register_lookup['OUTPUT_CONFIG'] = {'size': 9, 'OutputFmt': (0, 2), 'OutputActiveLanes': (2, 6), 'OutputSerType': (6, 7), 'OutputDataReadDelay': (7, 9)}
    global_register_lookup['CDR_CONFIG'] = {'size': 14, 'CdrSelSerClk': (0, 3), 'CdrVcoGain': (3, 6), 'CdrEnGck': (6, 7), 'CdrPdDel': (7, 11), 'CdrPdSel': (11, 13), 'CdrSelDelClk': (13, 14)}
    global_register_lookup['SER_SEL_OUT'] = {'size': 8, 'SerSelOut0': (0, 2), 'SerSelOut1': (2, 4), 'SerSelOut2': (4, 6), 'SerSelOut3': (6, 8)}
    global_register_lookup['CML_CONFIG'] = {'size': 8, 'CmlEn': (0, 4), 'CmlEnTap': (4, 6), 'CmlInvTap': (6, 8)}
    global_register_lookup['AURORA_CC_CONFIG'] = {'size': 8, 'AuroraCcSend': (0, 2), 'AuroraCcWait': (2, 8)}
    global_register_lookup['AURORA_CB_CONFIG0'] = {'size': 8, 'AuroraCbSend': (0, 4), 'AuroraCbWaitLow': (4, 8)}
    global_register_lookup['MONITOR_SELECT'] = {'size': 14, 'MonitorVmonMux': (0, 7), 'MonitorImonMux': (7, 13), 'MonitorEnable': (13, 14)}

    print('trim_register_map = {')
    for key, value in trim_register_lookup.items():
        if type(value) is str:
            print("    '{key}': '{value}',".format(key=key, value=value))
        elif type(value) is dict:
            print("    '{key}': {value},".format(key=key, value=value))

    print('}\n\nafe_register_map = {')
    for key, value in afe_register_lookup.items():
        if type(value) is str:
            print("    '{key}': '{value}',".format(key=key, value=value))
        elif type(value) is dict:
            print("    '{key}': {value},".format(key=key, value=value))

    print('}\n\nglobal_register_map = {')
    for key, value in global_register_lookup.items():
        if type(value) is str:
            print("    '{key}': '{value}',".format(key=key, value=value))
        elif type(value) is dict:
            print("    '{key}': {value},".format(key=key, value=value))

    print('}')

    return trim_register_lookup, afe_register_lookup, global_register_lookup


def find_differences(bdaq_config, yarr_config, use_trimbit_registers=True, use_afe_registers=True, use_global_registers=True):
    '''
        Compares two configuration files and lists the differences.

        Parameters
        ----------
        bdaq_config : dict
            BDAQ configuration dictionary on base level.

        yarr_config : dict
            YARR configuration dictionary on base level.

        Returns
        -------
        differences : dict
            List of all registers with differing values. Key is BDAQ register name, value is a tuple (BDAQ value, YARR value).
    '''

    differences = {}
    por_defaults = get_por_defaults()

    if use_trimbit_registers:
        log.info('Comparing trimbit settings...')
        for bdaq_name, yarr_name in trim_register_map.items():
            try:
                if bdaq_config['trim'][bdaq_name] != yarr_config['GlobalConfig'][yarr_name]:
                    differences[bdaq_name] = (bdaq_config['trim'][bdaq_name], yarr_config['GlobalConfig'][yarr_name])
            except KeyError as e:
                log.warning('One configuration has no value for register {0}!'.format(e))

    if use_afe_registers:
        log.info('Comparing AFE settings...')
        for bdaq_name, yarr_name in afe_register_map.items():
            if type(yarr_name) is str:
                try:
                    if bdaq_config['registers'][bdaq_name] != yarr_config['GlobalConfig'][yarr_name]:
                        differences[bdaq_name] = (bdaq_config['registers'][bdaq_name], yarr_config['GlobalConfig'][yarr_name])
                except KeyError as e:
                    log.warning('One configuration has no value for register {0}!'.format(e))
            elif type(yarr_name) is dict:
                values = {}
                for yarr_subname in yarr_name.keys():
                    if yarr_subname not in ['size']:
                        values[yarr_subname] = yarr_config['GlobalConfig'][yarr_subname]
                converted_yarr_value = _combine_conf_registers(yarr_name, values)
                try:
                    if bdaq_config['registers'][bdaq_name] != converted_yarr_value:
                        differences[bdaq_name] = (bdaq_config['registers'][bdaq_name], converted_yarr_value)
                except KeyError as e:
                    log.warning('One configuration has no value for register {0}!'.format(e))

    if use_global_registers:
        log.info('Comparing global register settings...')
        for bdaq_name, yarr_name in global_register_map.items():
            if type(yarr_name) is str:
                try:
                    if bdaq_config['registers'][bdaq_name] != yarr_config['GlobalConfig'][yarr_name]:
                        differences[bdaq_name] = (bdaq_config['registers'][bdaq_name], yarr_config['GlobalConfig'][yarr_name])
                except KeyError as e:
                    log.warning('One configuration has no value for register {0}!'.format(e))
            elif type(yarr_name) is dict:
                values = {}
                for yarr_subname in yarr_name.keys():
                    if yarr_subname not in ['size']:
                        values[yarr_subname] = yarr_config['GlobalConfig'][yarr_subname]
                converted_yarr_value = _combine_conf_registers(yarr_name, values)
                try:
                    if bdaq_config['registers'][bdaq_name] != converted_yarr_value:
                        differences[bdaq_name] = (bdaq_config['registers'][bdaq_name], converted_yarr_value)
                except KeyError as e:
                    log.warning('One configuration has no value for register {0}!'.format(e))

    # TODO: Compare parameter section -> InjCap?

    if len(differences) == 0:
        log.success('Configurations are identical!')
    else:
        log.warning('Configurations differ in {0} cases:'.format(len(differences)))
        for reg, (bdaq_value, yarr_value) in differences.items():
            try:
                log.warning('{0}: POR = {1}, BDAQ53 = {2}, YARR = {3}'.format(reg, por_defaults[reg], bdaq_value, yarr_value))
            except KeyError:
                log.warning('{0}: BDAQ53 = {1}, YARR = {2}'.format(reg, bdaq_value, yarr_value))

    return differences


def get_por_defaults():
    '''
        Get on-chip default values from register definition yaml.
    '''
    with open(os.path.join(root_dir, 'rd53a_registers.yaml'), 'r') as f:
        register_list = yaml.safe_load(f)['registers']

    defaults = {}
    for reg in register_list:
        defaults[reg['name']] = int(reg['default'], 2)

    return defaults

#
# YARR converter
#


def get_yarr_default_config(yarr_config_url):
    '''
        Download an up-to-date YARR default config from YARR Gitlab.

        Parameters
        ----------
        None

        Returns
        -------
        config : dict
            YARR configuration dict on base level.
    '''

    page = requests.get(yarr_config_url)
    if page.status_code == 200:
        config = json.loads(page.content.decode('utf-8'))['RD53A']
    else:
        log.warning('Cannot access YARR Gitlab page! Using fallback default YARR configuration.')
        config = yarr_fallback_config
        masks = {'Enable': 0, 'Hitbus': 1, 'InjEn': 1, 'TDAC': 1}
        for col in range(n_cols):
            config['PixelConfig'].append({'Col': col})
            for mask, default in masks.items():
                config['PixelConfig'][-1][mask] = [default for _ in range(n_rows)]

    return config


def read_yarr_config(config_file):
    '''
        Read YARR configuration file.

        Parameters
        ----------
        config_file : str
            Path to YARR-style JSON file.

        Returns
        -------
        config : dict
            YARR configuration dict on base level.
    '''

    log.info('Reading configuration from YARR file {0}'.format(config_file))
    with open(config_file, 'r') as f:
        config = json.load(f)

    return config


def write_yarr_config(config, outfile, update=['GlobalConfig', 'Parameter', 'PixelConfig']):
    '''
        Write YARR configuration to YARR-style JSON file.

        Parameters
        ----------
        config : dict
            YARR cofniguration dictionary on 'RD53A' level.

        outfile : str
            Output filename or -path.

        update : list
            YARR configuration nodes to overwrite if outfile already exists.

        Returns
        -------
        None
    '''

    outfile += '.json'
    new_config = {}
    new_config['_comment'] = 'This file was automatically converted from a BDAQ53 configuration file.'
    log.info('Writing configuration to YARR file {0}'.format(outfile))

    if os.path.isfile(outfile):
        log.warning('Outfile already existing. Overwriting keys: {0}'.format(update))

        with open(outfile, 'r') as f:
            original_config = json.load(f)['RD53A']
        for key in update:
            original_config[key] = config[key]
        new_config['RD53A'] = original_config
    else:
        new_config['RD53A'] = config

    with open(outfile, 'w') as f:
        json.dump(new_config, f, indent=4)


def read_bdaq_config(config_file):
    '''
        Read BDAQ configuration file.

        Parameters
        ----------
        config_file : str
            Path to BDAQ-style YAML file.

        Returns
        -------
        config : dict
            BDAQ configuration dict on base level.
    '''

    log.info('Reading configuration from BDAQ53 file {0}'.format(config_file))
    with open(config_file, 'r') as f:
        config = yaml.safe_load(f)

    return config


def write_bdaq_config(config, outfile):
    '''
        Write BDAQ configuration to BDAQ-style YAML file.

        Parameters
        ----------
        config : dict
            BDAQ configuration dictionary on base level.

        outfile : str
            Output filename or -path.

        Returns
        -------
        None
    '''

    outfile += '.cfg.yaml'
    log.info('Writing configuration to BDAQ53 file {0}'.format(outfile))
    with open(outfile, 'w') as f:
        f.write('# This file was automatically converted from a YARR configuration file.\n\n')
        yaml.dump(config, f)


def read_bdaq_masks(mask_file):
    '''
        Read BDAQ mask file.

        Parameters
        ----------
        mask_file : str
            Path to BDAQ-style H5 file.

        Returns
        -------
        masks : dict
            BDAQ mask dict of numpy arrays.
    '''

    log.info('Reading masks from BDAQ53 file {0}'.format(mask_file))
    masks = _make_default_masks(n_cols, n_rows)
    with tb.open_file(mask_file, 'r') as f:
        for mask_type in masks.keys():
            for node in f.list_nodes('/masks/', classname='CArray'):
                if node.name == mask_type:
                    masks[mask_type][:] = node[:]
                    break

    return masks


def write_bdaq_masks(masks, outfile):
    '''
        Write BDAQ mask file.

        Parameters
        ----------
        masks : dict
            BDAQ mask dictionary of numpy arrays.

        outfile : str
            File name or path of BDAQ-style H5 file.

        Returns
        -------
        None
    '''

    outfile += '.masks.h5'
    log.info('Writing masks to BDAQ53 file {0}'.format(outfile))
    with tb.open_file(outfile, 'w') as f:
        f.create_group(f.root, 'masks', 'Masks')

        for mask_type, data in masks.items():
            f.create_carray(f.root.masks,
                            name=mask_type,
                            title=mask_type,
                            obj=data,
                            filters=tb.Filters(complib='blosc',
                                               complevel=5,
                                               fletcher32=False))


def _combine_conf_registers(df, values):
    '''
        Combine YARR registers:
            SyncAutoZero, SyncSelC2F, SyncSelC4F, SyncFastTot to BDAQ register CONF_FE_SYNC or
            DiffLccEn, DiffFbCapEn to BDAQ register CONF_FE_DIFF.

        Parameters
        ----------
        df : dict
            Definition of subregisters. This is the value of the corresponding BDAQ register in the register name map.

        values : dict
            Dictionary with subregister values.

        Returns
        -------
        bdaq_value : int
            The BDAQ register value.
    '''

    size = df.pop('size')
    bdaq_value = list(format(0, '0{0}b'.format(size)))
    for yarr_subname, (start, stop) in df.items():
        subsize = stop - start
        bdaq_value[start:stop] = format(values[yarr_subname], '0{0}b'.format(subsize))[::-1]

    return int(''.join(bdaq_value[::-1]), 2)


def _split_conf_registers(df, bdaq_value):
    '''
        Split BDAQ registers:
            CONF_FE_SYNC to YARR registers SyncAutoZero, SyncSelC2F, SyncSelC4F, SyncFastTot or
            CONF_FE_DIFF to YARR registers DiffLccEn, DiffFbCapEn.

        Parameters
        ----------
        df : dict
            Definition of subregisters. This is the value of the corresponding BDAQ register in the register name map.

        bdaq_value : dict
            Dictionary with subregister values.

        Yields
        -------
        subreg : str
            Name of the subregister.

        value : int
            Value of the subregister.
    '''

    size = df.pop('size')
    value_string = format(bdaq_value, '0{0}b'.format(size))

    for subreg, (start, stop) in df.items():
        yield subreg, int(value_string[start:stop], 2)


def convert_config_yarr_to_bdaq(yarr_config, use_trimbit_registers=True, use_afe_registers=True, use_global_registers=False):
    '''
        Create chip configuration for BDAQ53 from YARR configuration.

        Parameters
        ----------
        yarr_config : dict
            Dictionary containing YARR configuration on register level. (config.json['RD53A']['GlobalConfig']).

        Returns
        -------
        config : dict
            BDAQ chip configuration dict
    '''

    # Converting global register settings will probably lead to unexpected behavior
    if use_global_registers:
        log.warning('Default global register settings are not compatible. This might lead to unexpected behavior!')

    # Load default config file
    with open(bdaq_default_config_path, mode='r') as f:
        config = yaml.safe_load(f)

    # Add YARR section to config for registers that should not be used by BDAQ but kept in case of conversion back to YARR
    if not (use_trimbit_registers and use_afe_registers and use_global_registers):
        config['yarr'] = {}

    # Update Trimbit registers
    for reg, yarr_name in trim_register_map.items():
        if type(yarr_name) == str:
            try:
                if use_trimbit_registers:
                    config['trim'][reg] = yarr_config[yarr_name]
                else:
                    config['yarr'][reg] = yarr_config[yarr_name]
            except KeyError as e:
                log.warning('No data for register {0} in input config file!'.format(e))

    # Update AFE registers
    for reg, yarr_name in afe_register_map.items():
        if type(yarr_name) == str:
            try:
                if use_afe_registers:
                    config['registers'][reg] = yarr_config[yarr_name]
                else:
                    config['yarr'][reg] = yarr_config[yarr_name]
            except KeyError as e:
                log.warning('No data for register {0} in input config file!'.format(e))
        elif type(yarr_name) == dict:
            values = {}
            for yarr_subname in yarr_name.keys():
                if yarr_subname not in ['size']:
                    values[yarr_subname] = yarr_config[yarr_subname]
            config['registers'][reg] = _combine_conf_registers(yarr_name, values)

    # Update global registers
    for reg, yarr_name in global_register_map.items():
        if type(yarr_name) == str:
            try:
                if use_global_registers:
                    config['registers'][reg] = yarr_config['RD53A']['GlobalConfig'][yarr_name]
                else:
                    config['yarr'][reg] = yarr_config['RD53A']['GlobalConfig'][yarr_name]
            except KeyError as e:
                log.warning('No data for register {0} in input config file!'.format(e))

    # TODO: Update parameter section -> InjCap?

    return config


def convert_config_bdaq_to_yarr(bdaq_config, use_trimbit_registers=True, use_afe_registers=True, use_global_registers=False):
    '''
        Create chip configuration for YARR from BDAQ53 configuration.

        Parameters
        ----------
        bdaq_config : dict
            Dictionary containing BDAQ configuration on base level. (config.cfg.yaml).

        Returns
        -------
        config : dict
            YARR chip configuration dict
    '''

    # Converting global register settings will probably lead to unexpected behavior
    if use_global_registers:
        log.warning('Default global register settings are not compatible. This might lead to unexpected behavior!')

    # Load default config file
    config = get_yarr_default_config(yarr_default_config_url)

    # Update Trimbit registers
    if use_trimbit_registers:
        for bdaq_name, yarr_name in trim_register_map.items():
            try:
                config['GlobalConfig'][yarr_name] = bdaq_config['trim'][bdaq_name]
            except KeyError as e:
                log.warning('No data for register {0} in input config file!'.format(e))

    # Update AFE registers
    if use_afe_registers:
        for bdaq_name, yarr_name in afe_register_map.items():
            if type(yarr_name) is str:
                try:
                    config['GlobalConfig'][yarr_name] = bdaq_config['registers'][bdaq_name]
                except KeyError as e:
                    log.warning('No data for register {0} in input config file!'.format(e))
            elif type(yarr_name) is dict:
                for subreg, value in _split_conf_registers(yarr_name, bdaq_config['registers'][bdaq_name]):
                    config['GlobalConfig'][subreg] = value

    # Update global registers
    if use_global_registers:
        for bdaq_name, yarr_name in global_register_map.items():
            try:
                config['GlobalConfig'][yarr_name] = bdaq_config['registers'][bdaq_name]
            except KeyError as e:
                log.warning('No data for register {0} in input config file!'.format(e))

    # TODO: Update parameter section -> InjCap?

    return config


def _make_default_masks(n_cols, n_rows):
    '''
        Create empty BDAQ mask dictionary.

        Parameters
        ----------
        n_cols : int
            Number of columns in the masks.

        n_rows : int
            Number of rows in the masks.

        Returns
        -------
        masks : dict
            Dict containing empty numpy arrays for masks
    '''

    masks = {}
    masks['enable'] = np.zeros((n_cols, n_rows), dtype=bool)
    masks['disable'] = np.ones((n_cols, n_rows), dtype=bool)
    masks['injection'] = np.zeros((n_cols, n_rows), dtype=bool)
    masks['hitbus'] = np.zeros((n_cols, n_rows), dtype=bool)
    masks['tdac'] = np.zeros((n_cols, n_rows), dtype=int)
    masks['lin_gain_sel'] = np.zeros((n_cols, n_rows), dtype=bool)
    masks['injection_delay'] = np.full((n_cols, n_rows), 351, dtype=int)

    return masks


def convert_masks_yarr_to_bdaq(yarr_config, use_enable_mask=True, use_injection_mask=False, use_hitbus_mask=False, use_tdac_mask=True):
    '''
        Create masks for BDAQ53 from YARR configuration.

        Parameters
        ----------
        yarr_config : dict
            Dictionary containing YARR configuration on mask level (config.json['RD53A']['PixelConfig']).

        Returns
        -------
        masks : dict
            Dict containing numpy arrays for masks
    '''

    update_masks = []
    if use_enable_mask:
        update_masks.append(('disable', 'Enable'))
    if use_hitbus_mask:
        update_masks.append(('hitbus', 'Hitbus'))
    if use_injection_mask:
        update_masks.append(('injection', 'InjEn'))
    if use_tdac_mask:
        update_masks.append(('tdac', 'TDAC'))

    log.info('Converting masks, using: {0}'.format([t[1] for t in update_masks]))

    masks = _make_default_masks(n_cols, n_rows)

    for col_data in yarr_config:
        col = col_data['Col']
        for mask_type in update_masks:
            if (mask_type[1] == 'Enable' and not use_enable_mask) or (mask_type[1] == 'InjEn' and not use_injection_mask) or (mask_type[1] == 'Hitbus' and not use_hitbus_mask) or (mask_type[1] == 'TDAC' and not use_tdac_mask):
                continue
            masks[mask_type[0]][col, :] = col_data[mask_type[1]]

    return masks


def convert_masks_bdaq_to_yarr(bdaq_masks, use_enable_mask=True, use_injection_mask=False, use_hitbus_mask=False, use_tdac_mask=True):
    '''
        Create masks for YARR from BDAQ53 mask file.

        Parameters
        ----------
        bdaq_masks : dict of numpy arrays
            Dictionary containing BDAQ masks.

        Returns
        -------
        masks : dict
            Dict containing masks in YARR notation.
    '''

    # Load default config file
    config = get_yarr_default_config(yarr_default_config_url)
    original_masks = config.pop('PixelConfig')

    update_masks, keep_masks = [], []
    if use_enable_mask:
        update_masks.append(('disable', 'Enable'))
    else:
        keep_masks.append('Enable')
    if use_hitbus_mask:
        update_masks.append(('hitbus', 'Hitbus'))
    else:
        keep_masks.append('Hitbus')
    if use_injection_mask:
        update_masks.append(('injection', 'InjEn'))
    else:
        keep_masks.append('InjEn')
    if use_tdac_mask:
        update_masks.append(('tdac', 'TDAC'))
    else:
        keep_masks.append('TDAC')

    log.info('Converting masks, using: {0}'.format([t[1] for t in update_masks]))

    config['PixelConfig'] = []
    for col in range(n_cols):
        column_dict = {'Col': col}
        for (bdaq_mask_name, yarr_mask_name) in update_masks:
            column_dict[yarr_mask_name] = [int(val) for val in bdaq_masks[bdaq_mask_name][col, :]]
            for column_data in original_masks:
                if column_data['Col'] == col:
                    for yarr_mask_name in keep_masks:
                        column_dict[yarr_mask_name] = column_data[yarr_mask_name]
        config['PixelConfig'].append(column_dict)

    return config


#
# Config generation and DB query
#


def generate_config(chip_sn, database='rd53', outfile=None, **_):
    '''
    Generate a BDAQ configuration file from database data.

    Parameters
    ----------
    chip_sn : str
        Chip serial number to use for query. Format '0xWWCR.

    database : str
        Database to query. So far only 'rd53' is supported to query the RD53 internal database.

    outfile : str
        File name or path of output file. If None, the file will be created in [BDAQ root dir]/[Chip SN].cfg.yaml.

    Returns
    -------
    None
    '''

    log.info('Generating configuration file for chip {0}'.format(chip_sn))
    if outfile is None:
        outfile = os.path.join(root_dir, '{chip_sn}.cfg.yaml'.format(chip_sn=chip_sn))

    if database == 'rd53':
        data = manage_databases.query_rd53a_db(chip_sn)
    else:
        log.error('Unsuported database: {0}'.format(database))
        return

    with open(bdaq_default_config_path, 'r') as f:
        lines = f.readlines()

    with open(outfile, 'w') as f:
        for idx, line in enumerate(lines):
            line = line.strip('\n')
            if 'VREF_A_TRIM' in line and 'VREF_A_TRIM' in data.keys() and data['VREF_A_TRIM'] is not None:
                line = line[:-2] + str(data['VREF_A_TRIM'])
            if 'VREF_D_TRIM' in line and 'VREF_D_TRIM' in data.keys() and data['VREF_D_TRIM'] is not None:
                line = line[:-2] + str(data['VREF_D_TRIM'])
            if 'MON_BG_TRIM' in line and 'MON_BG_TRIM' in data.keys() and data['MON_BG_TRIM'] is not None:
                line = line[:-2] + str(data['MON_BG_TRIM'])
            if 'MON_ADC_TRIM' in line and 'MON_ADC_TRIM' in data.keys() and data['MON_ADC_TRIM'] is not None:
                line = line[:-1] + str(data['MON_ADC_TRIM'])

            if ' slope ' in line and 'e_conversion' in lines[idx - 2] and 'VCAL_SLOPE' in data.keys() and data['VCAL_SLOPE'] is not None:
                line = line[:-4] + str(data['VCAL_SLOPE'])
            if ' offset ' in line and 'e_conversion' in lines[idx - 5] and 'VCAL_OFFSET' in data.keys() and data['VCAL_OFFSET'] is not None:
                line = line[:-3] + str(data['VCAL_OFFSET'])

            f.write(line + '\n')

    log.success('Done! New config file saved as {0}'.format(outfile))


#
# CLI definition
#


def main():
    parser = argparse.ArgumentParser(description='Compare, convert or generate BDAQ53 configuration files.',
                                     usage="""bdaq_config <command>

        Possible commands are:
            generate    Generate a fresh configuration file from database.
            convert     Convert between YARR and BDAQ53 configuration formats.
            compare     Compare YARR and BDAQ53 configuration file and print registers with different values.""")
    parser.add_argument('command', type=str, help="Subcommand to execute.")

    args = parser.parse_args(sys.argv[1:2])

    if args.command in ['generate', 'make', 'download']:
        generate()

    elif args.command in ['convert']:
        convert()

    elif args.command in ['compare']:
        compare()


def generate():
    parser = argparse.ArgumentParser(description='Generate configuration files from database data.',
                                     usage="""bdaq_config generate [<options>]""")
    parser.add_argument('-c', '--chip_sn', type=str, help="Chip SN in format '0xWWCR' for generation of a fresh configuration. If not supplied, 'chip_sn' from your testbench.yaml is used.")
    parser.add_argument('-o', '--outfile', type=str, default=None, help="Ouput configuration filename.")
    parser.add_argument('-d', '--database', type=str, default='rd53', help="Database to query for data. So far only 'rd53' is supported to query the RD53 chip database.")

    args = parser.parse_args(sys.argv[2:])

    if args.chip_sn is None:
        # If there is no chip SN supplied, use chip SN specified in testbench.yaml
        with open(os.path.join(root_dir, 'testbench.yaml'), 'r') as tb:
            testbench = yaml.safe_load(tb)

        chip_sn = testbench['modules']['module_0']['chip_0']['chip_sn']

        log.info('No chip SN supplied. Using {0} from your testbench.yaml.'.format(chip_sn))
        args.chip_sn = chip_sn

    generate_config(**vars(args))


def convert():
    def _handle_file(infile, outfile, args):
        if infile.split('.')[-1] == 'json':    # Convert YARR to BDAQ
            yarr_config = read_yarr_config(infile)
            bdaq_config = convert_config_yarr_to_bdaq(yarr_config['RD53A']['GlobalConfig'], use_trimbit_registers=args.use_trimbit_registers, use_afe_registers=args.use_afe_registers, use_global_registers=args.use_global_registers)
            write_bdaq_config(bdaq_config, outfile)

            bdaq_masks = convert_masks_yarr_to_bdaq(yarr_config['RD53A']['PixelConfig'], use_enable_mask=args.use_enable_mask, use_injection_mask=args.use_injection_mask, use_hitbus_mask=args.use_hitbus_mask, use_tdac_mask=args.use_tdac_mask)
            write_bdaq_masks(bdaq_masks, outfile)

        elif infile.split('.')[-1] == 'yaml':  # Convert BDAQ config to YARR
            bdaq_config = read_bdaq_config(infile)
            yarr_config = convert_config_bdaq_to_yarr(bdaq_config, use_trimbit_registers=args.use_trimbit_registers, use_afe_registers=args.use_afe_registers, use_global_registers=args.use_global_registers)
            write_yarr_config(yarr_config, outfile, update=['GlobalConfig', 'Parameter'])

        elif infile.split('.')[-1] == 'h5':  # Convert BDAQ masks to YARR
            bdaq_masks = read_bdaq_masks(infile)
            yarr_masks = convert_masks_bdaq_to_yarr(bdaq_masks, use_enable_mask=args.use_enable_mask, use_injection_mask=args.use_injection_mask, use_hitbus_mask=args.use_hitbus_mask, use_tdac_mask=args.use_tdac_mask)
            write_yarr_config(yarr_masks, outfile, update=['PixelConfig'])

    parser = argparse.ArgumentParser(description='Convert configuration files between BDAQ53 and YARR.',
                                     usage="""bdaq_config convert <infile> [<options>]""")
    parser.add_argument('infile', nargs='+', type=str, default=None, help="Input config file for conversion. Has to be either YARR-style .json file or BDAQ-style .cfg.yaml / .masks.h5 file. Supplying one .cfg.yaml and one .masks.h5 file at the same time is supported to generate one YARR-style output file.")
    parser.add_argument('-o', '--outfile', type=str, default=None, help="Ouput configuration filename without filetype. If not specified, config will be generated next to infile.")
    parser.add_argument('--use_trimbit_registers', action='store_false', help="Use trimbit register settings from YARR config file in BDAQ? Defaults to True.")
    parser.add_argument('--use_afe_registers', action='store_false', help="Use AFE register settings from YARR config file in BDAQ? Defaults to True.")
    parser.add_argument('--use_global_registers', action='store_true', help="Use global register settings from YARR config file in BDAQ? Defaults to False.")
    parser.add_argument('--use_enable_mask', action='store_false', help="Use enable mask settings from YARR config file in BDAQ? Defaults to False.")
    parser.add_argument('--use_injection_mask', action='store_true', help="Use injection mask settings from YARR config file in BDAQ? Defaults to False.")
    parser.add_argument('--use_hitbus_mask', action='store_true', help="Use hitbus mask settings from YARR config file in BDAQ? Defaults to False.")
    parser.add_argument('--use_tdac_mask', action='store_false', help="Use TDAC mask settings from YARR config file in BDAQ? Defaults to True.")

    args = parser.parse_args(sys.argv[2:])

    if type(args.infile) == str:
        outfile = args.outfile
        if outfile is None:
            outfile = '.'.join(args.infile.split('.')[:-1])

        _handle_file(args.infile, outfile, args)

    elif type(args.infile) == list:
        outfile = args.outfile
        if outfile is None:
            outfile = '.'.join(args.infile[0].split('.')[:-1])
        elif type(outfile) == str:
            outfile = outfile.split('.')[0]
        else:
            raise IOError('Invalid output file: {0}'.format(outfile))

        for infile in args.infile:
            _handle_file(infile, outfile, args)


def compare():
    parser = argparse.ArgumentParser(description='Compare YARR and BDAQ53 configuration files.',
                                     usage="""bdaq_config compare <file1> <file2> [<options>]""")
    parser.add_argument('infile1', type=str, help="First configuration file for comparison. Has to be either YARR-style .json file or BDAQ-style .cfg.yaml file.")
    parser.add_argument('infile2', type=str, help="Second configuration file for comparison. Has to be either YARR-style .json file or BDAQ-style .cfg.yaml file.")
    parser.add_argument('--use_trimbit_registers', action='store_false', help="Compare trimbit register settings? Defaults to True.")
    parser.add_argument('--use_afe_registers', action='store_false', help="Compare AFE register settings? Defaults to True.")
    parser.add_argument('--use_global_registers', action='store_true', help="Compare global register settings? Defaults to False.")

    args = parser.parse_args(sys.argv[2:])

    fmt1 = args.infile1.split('.')[-1]
    if fmt1 == 'json':
        yarr_config = read_yarr_config(args.infile1)['RD53A']
        bdaq_config = read_bdaq_config(args.infile2)
    elif fmt1 == 'yaml':
        bdaq_config = read_bdaq_config(args.infile1)
        yarr_config = read_yarr_config(args.infile2)['RD53A']

    find_differences(bdaq_config, yarr_config, use_trimbit_registers=args.use_trimbit_registers, use_afe_registers=args.use_afe_registers, use_global_registers=args.use_global_registers)


if __name__ == '__main__':
    main()
