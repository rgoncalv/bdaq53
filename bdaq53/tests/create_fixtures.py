''' Allows automatic creation of new fixtures for failing tests

    Since fixtures are not that fix in fast developing code.
    Use with care and upload new fixtures only if you can ensure that you understand the
    changes and that the new fixtures are as expected!
'''

import argparse
import mock
import shutil
import os

import pytest

import bdaq53
from bdaq53.system import logger
from bdaq53.tests import utils


log = logger.setup_derived_logger('BDAQ fixture creator')

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


def main():
    software_test_path = os.path.join(bdaq53_path, 'tests', 'test_software')
    parser = argparse.ArgumentParser(description='BDAQ53 fixture creation', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('path', help="Optional folder or file name with tests to run", nargs='?', default=software_test_path)
    args = parser.parse_args()

    new_fixtures = []

    compare_h5_files_original = utils.compare_h5_files

    def compare_h5_files(first_file, second_file, node_names=None, ignore_nodes=None, detailed_comparison=True, exact=True, rtol=1e-5, atol=1e-8, chunk_size=1000000):
        checks_passed, error_msg = compare_h5_files_original(first_file, second_file, node_names, ignore_nodes, detailed_comparison, exact, rtol, atol, chunk_size)
        if not checks_passed:  # test failed --> use new data as fixture
            # Update the file in the fixture folder
            if data_folder in first_file:
                shutil.copy(second_file, first_file)
                new_fixtures.append(first_file)
            elif data_folder in second_file:
                shutil.copy(first_file, second_file)
                new_fixtures.append(second_file)
        return checks_passed, error_msg

    log.info('Create new fixtures for unit tests in %s' % args.path)

    with mock.patch('bdaq53.tests.utils.compare_h5_files', wraps=compare_h5_files):
        pytest.main([args.path])

    if new_fixtures:
        log.success('Created new fixture files: %s' % ', '.join(new_fixtures))
    else:
        log.success('All tests with fixtures passed. No new fixture was created.')


if __name__ == '__main__':
    main()
