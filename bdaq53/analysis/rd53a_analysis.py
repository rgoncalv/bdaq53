#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Collection of functions specific to the RD53A analysis
'''

from collections import OrderedDict

import numpy as np
import numba
from bdaq53.analysis import analysis_utils as au


def analyze_chunk(rawdata, return_hists=('HistOcc',), return_hits=False,
                  scan_param_id=0,
                  trig_pattern=0b11111111111111111111111111111111,
                  align_method=0,
                  analyze_tdc=False,
                  use_tdc_trigger_dist=False,
                  rx_id=0,
                  ext_trig_id_map=np.array([-1]),
                  ptot_table_stretched=np.array([0])):
    ''' Helper function to quickly analyze a data chunk.

        Warning
        -------
            If the rawdata contains incomplete event data only data that do
            not need event building are correct (occupancy + tot histograms)

        Parameters
        ----------
        rawdata : np.array, 32-bit dtype
            The raw data containing FE, trigger and TDC words
        return_hists : iterable of strings
            Names to select the histograms to return. Is not case sensitive
            and string must contain only e.g.: occ, tot, bcid, event
        return_hits : boolean
            Return the hit array
        scan_param_id : integer
            Set scan par id in hit info table
        trig_pattern : integer (32-bits)
            Indicate the position of the sub-triggers. Needed to check
            the BCIDs.
        align_method : integer
            Methods to do event alignment
            0: New event when number if event headers exceeds number of
               sub-triggers. Many fallbacks for corrupt data implemented.
            1: New event when data word is TLU trigger word, with error checks
            2: Force new event always at TLU trigger word, no error checks
        analyze_tdc : boolean
            If analyze_tdc is True, interpret and analyze also TDC words. Default is False,
            meaning that TDC analysis is skipped. This is useful for scans which do no
            require an TDC word interpretation (e.g. threshold scan) in order to save time.
        use_tdc_trigger_dist : boolean
            If True use trigger distance (delay between Hitor and Trigger) in TDC word
            interpretation. If False use instead TDC timestamp from TDC word. Default
            is False.
        rx_id : int
            ID of the Aurora receiver the chip is connected to

        Usefull for tuning. Analysis per scan parameter not possible.
        Chunks should not be too large.

        Returns
        -------
            ordered dict: With key = return_hists string, value = data
            and first entry are hits when selected
    '''

    n_hits = rawdata.shape[0] * 4
    (hits, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id, hist_event_status,
     hist_tdc_status, hist_tdc_value, hist_bcid_error, hist_ptot, hist_ptoa) = au.init_outs(n_hits, n_scan_params=1, analyze_tdc=analyze_tdc)

    interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_tdc_status, hist_tdc_value, hist_bcid_error,
                   hist_ptot, hist_ptoa,
                   scan_param_id=scan_param_id, event_number=0,
                   trig_pattern=trig_pattern,
                   align_method=align_method,
                   prev_trig_id=-1,
                   analyze_tdc=analyze_tdc,
                   use_tdc_trigger_dist=use_tdc_trigger_dist,
                   last_chunk=True,
                   rx_id=rx_id,
                   trigger_table_stretched=ext_trig_id_map)

    hists = {'occ': hist_occ,
             'tot': hist_tot,
             'rel': hist_rel_bcid,
             'event': hist_event_status,
             'error': hist_bcid_error
             }

    ret = OrderedDict()

    if return_hits:
        ret['hits'] = hits

    for key, value in hists.items():
        for word in return_hists:
            if key in word.lower():
                ret[word] = value
                break

    return ret


@numba.njit(cache=True, fastmath=True)
def interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_tdc_status, hist_tdc_value, hist_bcid_error,
                   hist_ptot, hist_ptoa,
                   scan_param_id, event_number=0,
                   trig_pattern=0b11111111111111111111111111111111,
                   align_method=0,
                   prev_trig_id=-1, prev_trg_number=-1, analyze_tdc=False,
                   use_tdc_trigger_dist=False, last_chunk=False, rx_id=0,
                   trigger_table_stretched=np.array([0]), ptot_table_stretched=np.array([0])):
    ''' Interprets raw data words to create hits and fill histograms.
        The FE 32-bit data is splitted into two 32-bit words with the
        FE data in the low word:
            1. 32-bit raw data:    16-bit + 16-bit FE high word
            2. 32-bit raw data:    16-bit + 16-bit FE low word

        One event is build for one external trigger! Triggers issued by
        RD53A are sub-triggers and data of these is combined to one event.

        Parameters
        ----------
        rawdata : numpy.array
            Raw data words of actual chunk
        event_number : integer
            The actual event number
        hits : numpy.recarray
            Hit array to be filled.
        hist_occ, hist_tot, hist_rel_bcid : np.array
            Histograms to be filled.
        scan_param_id : integer
            Actual scan parameter id. Added as hit info.
        trig_pattern : integer (32-bits)
            Indicate the position of the sub-triggers. Needed to check
            the BCIDs.
        align_method : integer
            Methods to do event alignment
            0: New event when number of event headers exceeds number of
               sub-triggers. Many fallbacks for corrupt data implemented.
            1: New event when data word is TLU trigger word with error checks
            2: Force new event always at TLU trigger word, no error checks
            FIXME: But two consecutive triggers (no event data) will not create a new event
        prev_bcid : integer
            BCID of last chunk. If -1 bcid of first header is used
        prev_trig_id : integer
            Trigger ID of last chunk. If -1 trigger ID of first header is used
        prev_trg_number : integer
            External trigger number of last chunk. If -1 trigger number of
            first trigger word is used
        analyze_tdc : boolean
            If analyze_tdc is True, interpret and analyze also TDC words. Default is False,
            meaning that TDC analysis is skipped. This is useful for scans which do no
            require an TDC word interpretation (e.g. threshold scan) in order to save time.
        use_tdc_trigger_dist : boolean
            If True use trigger distance (delay between Hitor and Trigger) in TDC word
            interpretation. If False use instead TDC timestamp from TDC word. Default
            is False.
        last_chunk : boolean
            The chunk is the last chunk. Needed to build last event
        rx_id : int
            ID of the Aurora receiver the chip is connected to
    '''

    fe_high_word = True
    data_header = False
    trg_header = False
    data_out_i = 0
    bcid = -1
    # Is set if a BCID offset is likely (e.g. too many event header in event)
    is_bcid_offset = False

    # Per call variables
    n_trigger = au.number_of_set_bits(trig_pattern)
    # Calculate effective not byte aligned pattern (= ommit leading zeros)
    eff_trig_patt = trig_pattern
    for _ in range(32):
        if eff_trig_patt & 0x80000000:
            break
        eff_trig_patt = eff_trig_patt << 1

    # Hit buffer to store actual event hits, needed to set parameters
    # calculated at the end of an event and not available before
    hit_buffer_i = 0  # Index of entry of hit buffer
    hit_buffer = np.zeros_like(hits)
    last_word_index = 0  # Last raw data index of full event

    # Per event variables
    trig_id = -1
    trig_tag = -1
    start_bcid = -1
    start_trig_id = -1
    event_status = 0
    n_event_header = 0
    prev_bcid = -1  # BCID before the expected BCID
    last_bcid = -1  # BCID of last event header
    i_pattern = 0  # index in trigger pattern
    trg_number = 0
    tdc_word_count = 0  # TDC word counter per event
    tdc_status = np.full(shape=(4,), fill_value=0, dtype=np.uint8)  # Status for each TDC line
    tdc_status_buffer = np.full(shape=(4,), fill_value=0, dtype=np.uint8)  # Array in order to buffer TDC status
    tdc_value = np.full(shape=(4,), fill_value=2 ** 16 - 1, dtype=np.uint16)  # TDC value for each Hitor line
    tdc_value_buffer = np.full(shape=(4,), fill_value=2 ** 16 - 1,
                               dtype=np.uint16)  # Array in order to buffer TDC values
    tdc_timestamp = np.zeros(shape=(4,),
                             dtype=np.uint16)  # TDC timestamp for each Hitor line. If use_trigger_dist is True, this will be trigger distance
    tdc_timestamp_buffer = np.zeros(shape=(4,), dtype=np.uint16)  # Array in order to buffer TDC timestamps
    tdc_word = np.full(shape=(4,), fill_value=False, dtype=np.bool_)  # Indicator for TDC word for each Hitor line
    event_status_tdc = 0  # Seperate variable for TDC event status in order to buffer the event status of TDC (TDC comes before actual event if no external trigger word is provided)
    i = 0

    for i, word in enumerate(rawdata):
        if word & au.TRIGGER_HEADER:
            trg_word_id = i
            trg_number = word & au.TRG_MASK
            event_status |= au.E_EXT_TRG
            trg_header = True
            # Special case for first trigger word
            if (align_method == 1 or align_method == 2):
                if i == 0:
                    prev_trg_number = trg_number
                    trg_header = False
            continue

        if au.is_tdc_word(word) and analyze_tdc:  # Select first TDC word (no matter if it comes from TDC1 - 3)
            tdc_word_count += 1  # Increase TDC word counter (per event).
            tdc_index = (word & 0x70000000) >> 28  # Get index of TDC module (Hitor line).
            if tdc_word[tdc_index - 1]:  # If event has already TDC word from the actual tdc index, set TDC ambiguous status.
                if not use_tdc_trigger_dist:
                    # Set TDC status per TDC line.
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_AMBIGUOUS
                elif (au.TDC_TRIG_DIST_MASK & word) >> 20 < 254:  # In case of trigger distance measurement, a valid trigger distance (smaller than 254) defines the valid TDC word
                    if tdc_timestamp_buffer[tdc_index - 1] < 254:
                        # Set TDC ambiguous if have already a valid TDC word (trigger distance < 254)
                        tdc_status_buffer[tdc_index - 1] |= au.H_TDC_AMBIGUOUS
                    else:
                        # If still had no valid, update TDC word with the first valid one
                        tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TRIG_DIST_MASK & word) >> 20
                        tdc_value_buffer[tdc_index - 1] = word & au.TDC_VALUE_MASK
                        if tdc_value_buffer[tdc_index - 1] != 0xFFF and not (tdc_timestamp_buffer[tdc_index - 1] == 254 and use_tdc_trigger_dist):
                            # Un-set TDC overflow in case found valid TDC word
                            tdc_status_buffer[tdc_index - 1] = tdc_status_buffer[tdc_index - 1] & ~au.H_TDC_OVF
                        if tdc_value_buffer[tdc_index - 1] != 0 and not (tdc_timestamp_buffer[tdc_index - 1] == 255 and use_tdc_trigger_dist):
                            # Un-set TDC error in case found valid TDC word
                            tdc_status_buffer[tdc_index - 1] = tdc_status_buffer[tdc_index - 1] & ~au.H_TDC_ERR
            else:  # Set for the first TDC word the corresponding event status
                # Do not use event status directly, in order to buffer TDC event status, since it comes before
                # the actual event (before EH0).
                event_status_tdc = au.E_TDC
                tdc_word[tdc_index - 1] = True  # Set TDC indicator for actual TDC index to True
                tdc_value_buffer[tdc_index - 1] = word & au.TDC_VALUE_MASK  # Extract the TDC value from TDC word
                if use_tdc_trigger_dist:
                    # If use_tdc_trigger_dist is True, extract trigger distance (delay between Hitor and Trigger) from TDC word
                    tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TRIG_DIST_MASK & word) >> 20
                else:
                    # If use_tdc_trigger_dist is False, extract TDC timestamp from TDC word
                    tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TIMESTAMP_MASK & word) >> 12

                # Set error/overflow
                if tdc_value_buffer[tdc_index - 1] == 0xFFF or (
                        tdc_timestamp_buffer[tdc_index - 1] == 254 and use_tdc_trigger_dist):
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_OVF
                if tdc_value_buffer[tdc_index - 1] == 0 or (
                        tdc_timestamp_buffer[tdc_index - 1] == 255 and use_tdc_trigger_dist):
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_ERR

            if i == 0 and align_method == 0:  # Special case if first word is TDC word. Only possible if no external trigger word is provided.
                event_status |= event_status_tdc
                for index in range(4):
                    tdc_value[index] = tdc_value_buffer[index]
                    tdc_timestamp[index] = tdc_timestamp_buffer[index]
                    tdc_status[index] = tdc_status_buffer[index]
                tdc_word[:] = False
                event_status_tdc = 0

            continue

        # Aurora receiver ID encoded in header must match rx_id of currently analyzed chip
        if ((word >> 20) & 0xf) != rx_id:
            event_status |= au.E_INV_RX_ID
            continue

        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.USERK_FRAME_ID:  # skip USER_K frame
            event_status |= au.E_USER_K
            continue

        # Low word has unset header bits
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) and not fe_high_word:
            event_status |= au.E_UNKNOWN_WORD
            continue

        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.HEADER_ID:  # Data header
            data_header = True
            fe_high_word = True

        # Reassemble full 32-bit FE data word from two alternating FPGA data words
        # that store 16-bit data in the high / low word
        if fe_high_word:
            data_word = word & 0xffff
            fe_high_word = False  # Next is low word
            continue  # Low word still missing
        else:
            data_word = data_word << 16 | word & 0xffff
            fe_high_word = True  # Next is high word

        if data_header:
            data_header = False

            bcid = data_word & au.BCID_MASK
            trig_id = (data_word >> 20) & 0x1f
            trig_tag = (data_word >> 15) & 0x1f

            # Special cases for first event/event header
            if start_bcid == -1:
                start_bcid = bcid
            if start_trig_id == -1:
                start_trig_id = trig_id
                if align_method == 0:
                    prev_trg_number = trg_number
                event_status |= event_status_tdc
                for index in range(4):
                    tdc_value[index] = tdc_value_buffer[index]
                    tdc_timestamp[index] = tdc_timestamp_buffer[index]
                    tdc_status[index] = tdc_status_buffer[index]
                tdc_value_buffer[:] = 2 ** 16 - 1
                tdc_timestamp_buffer[:] = 0
                tdc_status_buffer[:] = 0
                tdc_word[:] = False
                event_status_tdc = 0

            if is_new_event(n_event_header, n_trigger, start_trig_id, trig_id,
                            start_bcid, bcid, prev_bcid, last_bcid, trg_header,
                            event_status, is_bcid_offset,
                            method=align_method):
                # Set event errors of old event
                if n_event_header != n_trigger:
                    event_status |= au.E_STRUCT_WRONG
                if bcid < 0:
                    event_status |= au.E_STRUCT_WRONG
                if trg_header and not au.check_difference(prev_trg_number, trg_number, 31):
                    event_status |= au.E_EXT_TRG_ERR
                # Handle case of too many event header than expected by setting
                # flag to check BCIDs in next event more carfully (allow to exceed n_triggers event header)
                if au.check_difference(last_bcid, bcid, 15):
                    is_bcid_offset = True
                else:
                    is_bcid_offset = False

                if len(trigger_table_stretched) > 1:
                    scan_param_id = trigger_table_stretched[trg_number]
                data_out_i = au.build_event(hits, data_out_i,
                                            hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                            hist_event_status, hist_tdc_status, hist_tdc_value,
                                            hist_ptot, hist_ptoa,
                                            hit_buffer, hit_buffer_i, start_bcid,
                                            scan_param_id, event_status, tdc_status)

                event_number += 1

                # Calculate last_word_index in order to split properly the chunks
                if align_method == 2:
                    last_word_index = trg_word_id  # in case of force trg numer calculate offset always to last trigger word
                else:
                    if not trg_header:
                        if analyze_tdc:  # Since TDC words come before the actual event (before EH 0) need to set last_word_index to first TDC word
                            last_word_index = i - tdc_word_count - 1  # to get first TDC word
                        else:
                            last_word_index = i - 1  # to get high word
                    else:
                        if analyze_tdc:
                            # FIXME: i - tdc_word_count - 2 should also work.
                            # But 1 of 1000000 it can happen that trigger word comes in between TDC words.
                            last_word_index = trg_word_id  # to get trigger word
                        else:
                            last_word_index = i - 2  # to get trigger word

                # Reset per event variables
                hit_buffer_i = 0
                start_bcid = bcid
                start_trig_id = trig_id
                i_pattern = 0
                prev_trg_number = trg_number
                trg_header = False

                # Event header has wrong trigger id
                if prev_trig_id != -1 and not au.check_difference(prev_trig_id, trig_id, bits=5):
                    # Special case: Last event header is repeated, assume that the event header
                    # belongs to the old event --> BCID offset for this event
                    if au.check_difference(prev_trig_id, trig_id, bits=5, delta=0) and au.check_difference(last_bcid,
                                                                                                           bcid,
                                                                                                           bits=15,
                                                                                                           delta=0):
                        is_bcid_offset = True
                    event_status |= au.E_TRG_ID_INC_ERROR

                # If old event struture is wrong this event is likely wrong too
                # Assume trigger ID error if no external trigger is available
                if (event_status & au.E_STRUCT_WRONG and align_method == 0):
                    event_status = au.E_TRG_ID_INC_ERROR
                else:
                    event_status = 0
                n_event_header = 0

                # Set after reset of event variables the TDC event status and write tdc value and timestamp.
                # Needed since TDC words come before actual event.
                event_status |= event_status_tdc
                for index in range(4):
                    tdc_value[index] = tdc_value_buffer[index]
                    tdc_timestamp[index] = tdc_timestamp_buffer[index]
                    tdc_status[index] = tdc_status_buffer[index]
                # Reset TDC variables
                event_status_tdc = 0
                tdc_value_buffer[:] = 2 ** 16 - 1
                tdc_timestamp_buffer[:] = 0
                tdc_status_buffer[:] = 0
                tdc_word[:] = False
                tdc_word_count = 0

            else:  # data word is event header, but not the first of one event
                # Check sanity of BCID counter
                if prev_bcid != -1 and not au.check_difference(prev_bcid, bcid, 15):
                    # If event has already BCID error it is not sufficient to
                    # check difference to previous BCID. Check to first BCID
                    if event_status & au.E_BCID_INC_ERROR:
                        if not au.check_difference(start_bcid + n_event_header, bcid, 15, 0):
                            if 0 <= n_event_header < 32 or align_method == 2:
                                if 0 <= n_event_header < 32:
                                    hist_bcid_error[n_event_header] += 1
                                # In case number of event headers is not between 0 and 32, ignore this
                                # if force trigger number is True.
                                else:
                                    continue
                            else:
                                raise
                    else:
                        event_status |= au.E_BCID_INC_ERROR
                        if 0 <= n_event_header < 32 or align_method == 2:
                            if 0 <= n_event_header < 32:
                                hist_bcid_error[n_event_header] += 1
                            # In case number of event headers is not between 0 and 32, ignore this
                            # if force trigger number is True.
                            else:
                                continue
                        else:
                            raise
                # Check sanity of trigger ID counter
                if prev_trig_id != -1 and not au.check_difference(prev_trig_id, trig_id, 5):
                    event_status |= au.E_TRG_ID_INC_ERROR

            # Set event header counter variables
            n_event_header += 1
            prev_trig_id = trig_id
            last_bcid = bcid
            prev_bcid = bcid
            # Correct expected previous BCID by trigger pattern
            for i_pattern in range(i_pattern + 1, 32):
                # Search for next trigger (=1) in trigger pattern
                if eff_trig_patt & (0b1 << (31 - i_pattern)):
                    break
                # Special case: all other bit are all zero
                if ((eff_trig_patt << (i_pattern + 1)) & 0xFFFFFFFF) == 0:
                    break
                else:  # Increase expectation for a 0 in trigger pattern
                    prev_bcid += 1

        else:  # data word is hit data or unknown word
            if trig_id < 0:
                event_status |= au.E_STRUCT_WRONG

            hit_buffer_i, event_status = add_hits(data_word, hit_buffer, hit_buffer_i,
                                                  event_number, trg_number, tdc_value, tdc_timestamp,
                                                  trig_id, bcid, trig_tag, event_status)
    if last_chunk:
        if len(trigger_table_stretched) > 1:
            scan_param_id = trigger_table_stretched[trg_number]
        data_out_i = au.build_event(hits, data_out_i,
                                    hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                    hist_event_status, hist_tdc_status, hist_tdc_value,
                                    hist_ptot, hist_ptoa,
                                    hit_buffer, hit_buffer_i, start_bcid,
                                    scan_param_id, event_status, tdc_status)
        event_number += 1
        last_word_index = i + 1

    return data_out_i, event_number, last_word_index - rawdata.shape[0], start_trig_id - 1, prev_trg_number - 1


@numba.njit(cache=True, fastmath=True)
def add_hits(data_word, hit_buffer, hit_buffer_i, event_number, trg_number,
             tdc_value, tdc_timestamp, trg_id, bcid, trg_tag, event_status):

    ptot = 0  # No PToT available for RD53A
    ptoa = 0  # No PToA available for RD53A
    multicol = (data_word >> 26) & 0x3f
    region = (data_word >> 16) & 0x3ff

    # TODO: Do column, row and tot extraction outside this function
    for i in range(4):
        col, row = au.translate_mapping(multicol, region, i)
        tot = (data_word >> i * 4) & 0xf

        if col < 400 and row < 192:
            if tot != 255 and tot != 15:
                # Fill hit buffer with data. Need to encapsulate this into new function in order to overload it.
                au.fill_buffer(hit_buffer, hit_buffer_i, event_number, trg_number, tdc_value,
                               tdc_timestamp, ptoa, trg_id, bcid, trg_tag, col, row, tot, ptot)

                hit_buffer_i += 1

        else:
            event_status |= au.E_UNKNOWN_WORD

    return hit_buffer_i, event_status


@numba.njit(cache=True, fastmath=True)
def is_new_event(n_event_header, n_trigger, start_trig_id, trig_id,
                 start_bcid, bcid, prev_bcid, last_bcid, trg_header,
                 event_status, is_bcid_offset, method):
    ''' Detect new event by different methods.

        Note:
        -----
        Checks for new events are rather complex to handle all possible
        cases. They are tested with high coverage in unit tests, do not
        make quick chances without checking that event building still works!

        Methods to do event alignment, create new event if:
        0: - Number of event header (n_event_header) exceeds the
             number of sub-triggers used during data taking (n_trigger)
             AND
             - BCID offset flag is not set
               (happens if previous event has too many event headers)
               AND
               - Event has BCID increase error
                 (increases likelihood that event has previous event headers)
               AND
               - The actual BCID follows the last BCID
                 (increases likelihood that actual event has an additional BCID)
          OR
           - Trigger ID does not meet expectation: trigger_id = start_trig_id + n_event_header
             AND
             - The BCID is also different from expectation: bcid = prev_bcid + 1
             AND NOT
             - Both counters are off with different positive offset. Same positive offset
               is expected if event header(s) is not recognized
          OR
           - The event is flagged to have a not trust worthy trigger ID.
             (this is the case if the trigger ID jumps within the event or the
              predecessing event had a wrong structure)
             AND
             - The BCID is different from expectation bcid = start_bcid + n_event_header
               (this is effectively a fallback to BCID alignment if trigger ID
               is already not trust worthy)
        1: - Previous data word is TLU trigger word. This is expected to be at the beginning of an event.
             OR
              - Number of event header (n_event_header) exceeds the number of sub-triggers used during data taking (n_trigger)
                This is a fallback to event header alignment and needed for rare cases where more BCIDs are readout.
        2: - Previous data word is TLU trigger word. This is expected to be at the beginning of an event. No error
             checks on FE data is applied (like number of event header). Needed for event reconstruction from TB data with sync flavor.
             This flavor has a lot of bugs in the digital part.
    '''

    if method == 0:
        # Number of trigge in event exceeded
        if n_event_header >= n_trigger:
            # Check if previous events data header are likely in this event
            if not is_bcid_offset:
                return True
            else:
                # Event with header of old event must have BCID error and
                if (event_status & au.E_BCID_INC_ERROR and au.check_difference(last_bcid, bcid, bits=15)):
                    return False
                else:
                    return True

        # Trigger ID is wrong
        elif not au.check_difference(start_trig_id + n_event_header, value_2=trig_id, bits=5, delta=0):
            # BCID is also wrong
            if not au.check_difference(prev_bcid, bcid, bits=15, delta=1):
                # BCID is wrong by the same bunch offset as trigger ID
                if trig_id - start_trig_id == bcid - prev_bcid:
                    # BCID increased by little --> assume (few) missed event header continue with event
                    if au.check_max_difference(prev_bcid, bcid, bits=15, delta=n_trigger - n_event_header):
                        return False
                    # BCID decreased --> assume broken event and start new event
                    else:
                        return True
                else:  # BCID and trigger ID are wrong, without same relation, likely new event
                    return True
            else:  # BCID is ok, continue with event
                return False
        # Event trigger ID cannot be trusted
        elif (event_status & au.E_TRG_ID_INC_ERROR):
            # Use BCID to detect new event
            if not au.check_difference(prev_bcid, bcid, bits=15, delta=1):
                return True
            else:
                return False
        else:
            return False
    elif method == 1:
        if not trg_header:
            # Fallback to event header alginment. Needed for missing or not recognized
            # trigger words.
            if n_event_header >= n_trigger:
                return True
            else:
                return False
        else:
            return True
    elif method == 2:
        # Every trigger creates a new event, independent how FE data looks like
        if not trg_header:
            return False
        else:
            return True
    else:
        raise


def interpret_userk_data(rawdata):
    userk_data = np.zeros(shape=rawdata.shape[0],
                          dtype={
                              'names': ['AuroraKWord', 'Status', 'Data1', 'Data1_AddrFlag', 'Data1_Addr', 'Data1_Data',
                                        'Data0', 'Data0_AddrFlag', 'Data0_Addr', 'Data0_Data'],
                              'formats': ['uint8', 'uint8', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16',
                                          'uint16', 'uint16']})
    userk_word_cnt = 0
    userk_block_cnt = 0
    userk_framelength = 2
    block_temp = 0
    userk_data_i = 0

    for word in rawdata:
        if (word & au.USERK_FRAME_ID):
            if userk_word_cnt == 0:
                userk_word = word & 0xffff
            else:
                userk_word = userk_word << 16 | word & 0xffff

            if userk_block_cnt == 2 * userk_framelength - 1:
                block_temp = (userk_word & 0x3) << 32 | block_temp
                userk_block = userk_word >> 2
                Data1 = userk_block & 0x7ffffff
                Data0 = (block_temp >> 8) & 0x7ffffff
                userk_data[userk_data_i]['AuroraKWord'] = block_temp & 0xff
                userk_data[userk_data_i]['Status'] = (userk_block >> 30) & 0xf
                userk_data[userk_data_i]['Data1'] = Data1
                userk_data[userk_data_i]['Data1_AddrFlag'] = (Data1 >> 25) & 0x1
                userk_data[userk_data_i]['Data1_Addr'] = (Data1 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data1_Data'] = (Data1 >> 0) & 0xffff
                userk_data[userk_data_i]['Data0'] = Data0
                userk_data[userk_data_i]['Data0_AddrFlag'] = (Data0 >> 25) & 0x1
                userk_data[userk_data_i]['Data0_Addr'] = (Data0 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data0_Data'] = (Data0 >> 0) & 0xffff
                userk_data_i += 1

                userk_block_cnt = 0

            else:
                userk_block_cnt += 1

            # interpret received packet as user k
            if userk_word_cnt >= userk_framelength - 1:
                userk_word_cnt = 0
                block_temp = userk_word
            else:
                userk_word_cnt += 1

    return userk_data[:userk_data_i]


def print_raw_data(raw_data):
    ''' Print raw data with interpretation for debugging '''
    for i, word in enumerate(raw_data):
        if word & au.TRIGGER_HEADER:
            print('TRG {:<26} {:032b}'.format(*[word & au.TRG_MASK, word]))
            continue

        if word & au.TDC_HEADER == au.TDC_ID_0:
            print('TDC VAL0 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, word]))
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            print('TDC VAL1 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, word]))
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            print('TDC VAL2 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, word]))
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            print('TDC VAL3 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, word]))
            continue

        if word & au.USERK_FRAME_ID:
            print('U-K {:032b}'.format(*[word]) + '\t{:08x}'.format(*[word]))
            continue

        if word & au.HEADER_ID:  # data header
            data_header = True
            fe_high_word = True

        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if fe_high_word:
            data_word = word & 0xffff
            fe_high_word = False  # Next is low word
            continue  # Low word stll missing
        else:
            data_word = data_word << 16 | word & 0xffff
            fe_high_word = True  # Next is high word

        if data_header:
            # After data header follows heder less hit data
            data_header = False

            bcid = data_word & 0x7fff
            trig_id = (data_word >> 20) & 0x1f
            trig_tag = (data_word >> 15) & 0x1f

            print('EH  {:>8} {:>8} {:>8} {:032b}'.format(
                *[bcid, trig_id, trig_tag, data_word]))
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            print('HIT ', end='')
            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        print('{:>8} {:>8} {:>8}'.format(
                            *[col, row, tot]), end='')
                else:
                    print('UNKNOWN')
            print(' {:032b}'.format(data_word))
