#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Noise Tuning:
    - set the local threshold to maximum
    - lower global threshold and until disable some pixels
    - lower local threshold (slowly) and increase if see noise hits
'''

import numpy as np
from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import online as oa


scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'VTH_name': 'Vthreshold_LIN',
    'VTH_start': 350,
    'VTH_step': 1,
    'wait_cycles': 100,
    'limit': 0.9,

    # Noise occupancy scan parameters
    'n_triggers': 1e6,
    'min_occupancy': 10
}


class NoiseTuning(ScanBase):
    scan_id = 'noise_threshold_tuning'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        if start_column < 128:
            raise NotImplementedError('TDAC tuning is not necessary for SYNC flavor!')
        if start_column < 264 and stop_column > 264:
            raise NotImplementedError('Tuning more than one flavor at once is not implemented yet.')

        self.data.scan_range = ((start_column, stop_column), (start_row, stop_row))
        self.data.flavor = self.chip.get_flavor(stop_column - 1)

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][:, :] = False
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        self.data.hist_occ = oa.OccupancyHistogramming()

    def take_data(self):
        error = False
        with self.readout(scan_param_id=1, callback=self.analyze_data_online):
            for _ in self.data.steps:
                self.chip.write_command(self.data.trigger_data, repetitions=50000)

        # if self.bdaq.rx_channels[self.chip.receiver].LOST_COUNT:
        if self.fifo_readout.get_rx_fifo_discard_count(rx_channel=self.chip.receiver):
            error = True
            self.bdaq.rx_channels[self.chip.receiver].RESET_COUNTERS = 1

            self.bdaq.rx_channels[self.chip.receiver].RESET = 1     # TODO temporary fix needs firmware update
            while not self.bdaq.rx_channels[self.chip.receiver].RX_READY:
                pass

        hist_occ = self.data.hist_occ.get()
        return np.count_nonzero(hist_occ), hist_occ, error

    def log_status(self, n_total_hits, hit_occ, iteration=0):
        if not self.data.tdac_iterations:
            n_disabled_pixels = (~(self.chip.masks.disable_mask[self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]])).sum()
            n_total_pixels = (self.data.scan_range[0][1] - self.data.scan_range[0][0]) * (self.data.scan_range[1][1] - self.data.scan_range[1][0])
            self.data.r_disabled_pixels = float(n_disabled_pixels) / float(n_total_pixels) * 100.
            self.log.info('It. {0} at thr. {1} | {2} hit px. (max occ. {3}) | {4} ({5:1.2f}%) px. disabled'.format(
                iteration, self.data.vth, n_total_hits, np.max(hit_occ), n_disabled_pixels, self.data.r_disabled_pixels))
        else:
            this_tdac = self.chip.masks['tdac'][self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]].flatten()
            mean_tdac = np.mean(this_tdac)
            if self.data.min_tdac < 0:
                this_tdac = this_tdac + 15
            tdac_dist = np.bincount(this_tdac)
            np.set_printoptions(linewidth=200)
            self.log.info('Mean TDAC is {0:1.2f}, distribution:\n{1}'.format(mean_tdac, str(tdac_dist)))

    def _scan(self, VTH_name='Vthreshold_LIN', VTH_start=350, VTH_step=1, n_triggers=1e6, min_occupancy=10, wait_cycles=200, limit=1, **_):
        '''
        Parameters
        ----------
        VTH_name : str
            Name of the threshold register of the flavor to tune.
        VTH_start : int
            Global threshold setting to start decreasing from.
        VTH_step : int
            Stepsize to decrease global threshold by.
        n_triggers : int
            Amount of triggers to send at every iteration.
        min_occupancy : int
            Minimum number of hits to count a pixel as noisy.
        wait_cycles : int
            Time between injections.
        limit : int
            Maximum amount of pixels to disable in percent.
        '''

        self.data.min_tdac, self.data.max_tdac, self.data.tdac_range, self.data.tdac_incr = self.chip.get_tdac_range(self.data.flavor)

        if self.data.flavor == 'SYNC':
            # Send ECR only for SYNC
            self.data.trigger_data = self.chip.inject_analog_single(send_ecr=True, wait_cycles=wait_cycles, write=False)
        else:
            self.data.trigger_data = self.chip.inject_analog_single(send_ecr=False, wait_cycles=wait_cycles, write=False)

        n = int(n_triggers / 32)  # Trigger command will always send 32 triggers
        self.data.steps = []
        while n > 0:
            if n >= 50000:
                self.data.steps.append(50000)
                n -= 50000
            else:
                self.data.steps.append(n)
                n -= n

        # disable injection
        self.chip.enable_macro_col_cal(macro_cols=None)

        if self.data.flavor != 'SYNC':
            self.chip.masks['tdac'][:, :] = self.data.max_tdac

        self.chip.masks.update()

        self.log.info('Finding lowest global threshold...')
        self.data.tdac_iterations = False

        for vth in range(VTH_start, 0, -1 * VTH_step):
            self.data.vth = vth
            self.chip.registers[VTH_name].write(vth)

            iteration = 0
            while True:
                n_hit_pixels, hit_occ, error = self.take_data()
                for col in range(*self.data.scan_range[0]):
                    for row in range(*self.data.scan_range[1]):
                        if hit_occ[col, row] >= min_occupancy:
                            self.chip.masks.disable_mask[col, row] = False

                self.chip.masks.apply_disable_mask()
                self.chip.masks.update()

                self.log_status(n_hit_pixels, hit_occ, iteration)
                iteration += 1

                if np.max(hit_occ) < min_occupancy and not error:  # no errors ? this is bad?
                    break

            if self.data.r_disabled_pixels >= limit:
                break

        self.data.vth += 1  # Increase global threshold -> To be adjusted
        self.log.success('Lowest stable global threshold value: {0} = {1}'.format(VTH_name, self.data.vth))
        self.chip.configuration['registers'][VTH_name] = self.data.vth
        self.chip.registers[VTH_name].write(self.data.vth)  # write threshold

        if self.data.flavor != 'SYNC':
            self.log.info('Starting TDAC scan. Finding lowest TDAC threshold...')
            self.data.tdac_iterations = True

            self.data.fix_tdac = np.ones((400, 192), dtype=bool)
            self.data.fix_tdac[self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]] = 0

            lower_tdac_step = 2  # LIN -> lower threshold for half of pixels
            if self.data.flavor == 'DIFF':
                lower_tdac_step = 1

            for iteration in range(self.data.tdac_range):
                # lower TDAC for not fixed in steps
                for step in range(lower_tdac_step):
                    lower_tdac = np.zeros((400, 192), dtype=bool)
                    lower_tdac[step::lower_tdac_step] = True
                    lower_tdac_2d = np.reshape(lower_tdac, (400, 192))

                    for col in range(*self.data.scan_range[0]):
                        for row in range(*self.data.scan_range[1]):
                            if lower_tdac_2d[col, row] and not self.data.fix_tdac[col, row]:
                                if self.data.flavor == 'LIN':
                                    if self.chip.masks['tdac'][col, row] < 15:
                                        self.chip.masks['tdac'][col, row] += 1
                                if self.data.flavor == 'DIFF':
                                    if self.chip.masks['tdac'][col, row] > -15:
                                        self.chip.masks['tdac'][col, row] -= 1

                    while True:
                        # update TDAC and take data
                        self.chip.masks.update()
                        n_total_hits, hit_occ, error = self.take_data()

                        # Adjust TDAC up if needed
                        for col in range(*self.data.scan_range[0]):
                            for row in range(*self.data.scan_range[1]):
                                if hit_occ[col, row]:  # and not self.data.fix_tdac[col, row]:  only once for TDAC do not adjust later the one set?

                                    # increase TDAC and fix
                                    self.data.fix_tdac[col, row] = True
                                    if self.data.flavor == 'LIN' and self.chip.masks['tdac'][col, row] > (iteration - 1):
                                        if self.chip.masks['tdac'][col, row] > 0:
                                            self.chip.masks['tdac'][col, row] -= 1

                                    if self.data.flavor == 'DIFF' and self.chip.masks['tdac'][col, row] < (16 - iteration):
                                        if self.chip.masks['tdac'][col, row] < 15:
                                            self.chip.masks['tdac'][col, row] += 1

                        self.log_status(n_total_hits, hit_occ, iteration)
                        # stop if max 1 hits in any pixel -> To be adjusted)
                        if self.data.flavor == 'DIFF':
                            if np.max(hit_occ[self.chip.masks['tdac'] < (16 - iteration)]) <= 1:
                                break
                        else:
                            if np.max(hit_occ[self.chip.masks['tdac'] > (iteration - 1)]) <= 1:
                                break

        n_disabled_pixels = (~(self.chip.masks.disable_mask[self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]])).sum()
        mean_tdac = np.mean(self.chip.masks['tdac'][self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]])
        self.log.success('Found optimal TDAC settings with a mean of {0:1.2f} and disabled {1} untunable pixels.'.format(mean_tdac, n_disabled_pixels))
        self.log.success('Lowest possible {0} is {1}.'.format(VTH_name, self.data.vth))

        self.data.hist_occ.close()  # stop analysis process
        self.log.success('Scan finished')

    # Used to overwrite data storing function: self.readout.handle_data
    def analyze_data_online(self, data_tuple, receiver=None):
        self.data.hist_occ.add(raw_data=data_tuple[0])
        super(NoiseTuning, self).handle_data(data_tuple, receiver)


if __name__ == '__main__':
    with NoiseTuning(scan_config=scan_configuration) as tuning:
        tuning.start()
