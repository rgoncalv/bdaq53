#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This scan sends triggers without injection
    into enabled pixels to identify noisy pixels.
'''

import time
import numpy as np
import scipy
import scipy.stats
import scipy.optimize
import tables as tb

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import plotting
from bdaq53.analysis import online as oa
from bdaq53.analysis import analysis_utils as au


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    # Start the scan with a fresh disable mask (resets pixels disabled by other scans)?
    'reset_disable_mask': False,

    'max_occupancy': 1e-6,  # Maximum noise occupancy per bunch crossing
    'certainty': 2.0,       # Expected/tolerable number of wrongly classified pixels (noisy vs. quiet). Note that amount of noise changes with time on a real chip.
    'stop_timeout': 20,     # Scan stops if number of unclassified pixels did not change for stop_timeout seconds. Hard cut is applied to remaining pixels.
    'max_scan_time': 120    # Maximum scan time to prevent not terminating scan. Hard cut is applied to remaining pixels.
}


class NoiseOccAdvScan(ScanBase):
    scan_id = 'noise_occupancy_scan_advanced'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, reset_disable_mask=False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.data.original_disable_mask = self.chip.masks.disable_mask.copy()

        self.chip.masks['injection'][:] = False
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        if reset_disable_mask:
            self.chip.masks.disable_mask[start_column:stop_column, start_row:stop_row] = True
            self.data.reset_disable_mask = self.chip.masks.disable_mask.copy()
            self.load_old_disable_mask = False
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update(force=True)

        # Disable injection
        self.chip.enable_macro_col_cal(macro_cols=None)

        self.data.hist_occ = oa.OccupancyHistogramming()
        self.data.occupancy = np.zeros(shape=(400, 192))
        self.data.noisy_map = np.full(shape=(400, 192), fill_value='-')

        # Do not abort on expecte RX errors
        self.configuration['bench']['general']['abort_on_rx_error'] = False

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, reset_disable_mask=False, n_triggers=1e9, max_occupancy=1e-6, certainty=1.0, stop_timeout=30, max_scan_time=120, wait_cycles=40, **_):
        '''
        Noise occupancy scan main loop

        Parameters
        ----------
        n_triggers : int
            Number of triggers to send.
        wait_cycles : int
            Time to wait in between trigger packages in units of sync commands.
        '''
        certainty = certainty / 2.
        quantile_alpha = certainty / 400 / 192

        def get_threshold_occupancies(N_TRIG, max_occ):

            def upper_quantile(N):
                chi2_dist = scipy.stats.chi2(2 * N + 2)
                return 0.5 * chi2_dist.ppf(1 - quantile_alpha)

            def lower_quantile(N):
                chi2_dist = scipy.stats.chi2(2 * N)
                return 0.5 * chi2_dist.ppf(quantile_alpha)

            def is_noisy_threshold(n_trig, max_occ):
                thr = n_trig * max_occ

                def root(N):
                    return lower_quantile(N) - thr
                result = scipy.optimize.root_scalar(f=root, args=(), method='bisect', bracket=[thr, 10 * thr], xtol=0.1)
                return result.root

            def is_quiet_threshold(n_trig, max_occ):
                thr = n_trig * max_occ

                def root(N):
                    return upper_quantile(N) - thr
                result = scipy.optimize.root_scalar(f=root, args=(), method='bisect', bracket=[0, thr], xtol=0.1)
                return result.root

            quiet_thr = int(is_quiet_threshold(N_TRIG, max_occ))
            noisy_thr = int(is_noisy_threshold(N_TRIG, max_occ))

            return quiet_thr, noisy_thr

        n_pixels = (stop_column - start_column) * (stop_row - start_row)

        trigger_data = self.chip.send_trigger(trigger=0b1111, write=False) * 8  # Effectively we send x32 triggers
        trigger_data += self.chip.write_sync_01(write=False) * wait_cycles

        trigger_bunch = 10000

        if start_column < 128:  # If SYNC enabled
            self.log.info('SYNC enabled: Using analog injection based command.')
            trigger_data = self.chip.inject_analog_single(send_ecr=True, wait_cycles=wait_cycles, write=False)

        number_bunch_crossings = 0

        num_unclassified_old = -1

        start_time = time.time()

        with self.readout(callback=self.analyze_data_online):
            pbar = tqdm(total=n_pixels, unit=' Classified Pixels', unit_scale=False)
            try:
                while True:
                    self.chip.write_command(trigger_data, repetitions=trigger_bunch)

                    number_bunch_crossings += trigger_bunch * 32
                    self.data.occupancy += self.data.hist_occ.get()

                    # Trigger at least 1e7 BCs before starting algorithm in order to prevent wrong classification at the beginning
                    # (i.e. collect at least O(10) noise hits of noisy pixels if max_occupancy is 1e-6)
                    if number_bunch_crossings < 1e7:
                        continue

                    try:
                        quiet_thr, noisy_thr = get_threshold_occupancies(number_bunch_crossings, max_occupancy)
                    except ValueError:
                        pass
                    else:
                        self.data.noisy_map[np.logical_and(self.data.noisy_map == '-', self.data.occupancy < quiet_thr)] = 'q'
                        self.data.noisy_map[np.logical_and(self.data.noisy_map == '-', self.data.occupancy > noisy_thr)] = 'n'

                    self.data.noise_occupancy_map = self.data.occupancy / float(number_bunch_crossings)

                    num_unclassified = np.count_nonzero(self.data.noisy_map[start_column:stop_column, start_row:stop_row] == '-')
                    num_noisy = np.count_nonzero(self.data.noisy_map[start_column:stop_column, start_row:stop_row] == 'n')
                    num_quiet = np.count_nonzero(self.data.noisy_map[start_column:stop_column, start_row:stop_row] == 'q')

                    if num_unclassified_old != num_unclassified:
                        timeout_start_time = time.time()

                        pbar.write('Quiet: {0}, Noisy: {1}, Unclassified: {2}'.format(num_quiet, num_noisy, num_unclassified))
                        pbar.n = n_pixels - num_unclassified
                        pbar.refresh()

                    num_unclassified_old = num_unclassified

                    self.chip.masks.disable_mask[self.data.noisy_map == 'n'] = False
                    self.chip.masks.apply_disable_mask()
                    self.chip.masks.update()

                    if num_unclassified == 0:
                        break

                    if num_unclassified > 1000:     # Never stop scan by timeout, if still many pixels unclassified
                        continue

                    # Using fixed cut for few remaining pixels; takes way too long otherwise.
                    if (stop_timeout and (time.time() - timeout_start_time > stop_timeout)) or (max_scan_time and (time.time() - start_time > max_scan_time)):
                        self.log.info('Timeout reached. Classifying remaining {0} pixels by simple cut at {1}'.format(num_unclassified, max_occupancy))
                        cut = number_bunch_crossings * max_occupancy
                        self.data.noisy_map[np.logical_and(self.data.noisy_map == '-', self.data.occupancy < cut)] = 'q'
                        self.data.noisy_map[np.logical_and(self.data.noisy_map == '-', self.data.occupancy > cut)] = 'n'
                        break
            except KeyboardInterrupt:
                self.log.info('Stopping scan due to keyboard interrupt. Classifying remaining {0} pixels by simple cut at {1}'.format(num_unclassified, max_occupancy))
                cut = number_bunch_crossings * max_occupancy
                self.data.noisy_map[np.logical_and(self.data.noisy_map == '-', self.data.occupancy < cut)] = 'q'
                self.data.noisy_map[np.logical_and(self.data.noisy_map == '-', self.data.occupancy > cut)] = 'n'

            pbar.close()

        self.data.hist_occ.close()

        disable_mask = np.ones((400, 192), dtype=bool)
        disable_mask[self.data.noisy_map == 'n'] = False

        if reset_disable_mask:
            self.chip.masks.disable_mask[start_column:stop_column, start_row:stop_row] = disable_mask[start_column:stop_column, start_row:stop_row]
        else:
            self.chip.masks.disable_mask[start_column:stop_column, start_row:stop_row] = np.logical_and(self.data.original_disable_mask[start_column:stop_column, start_row:stop_row], disable_mask[start_column:stop_column, start_row:stop_row])

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        if reset_disable_mask:
            n_disabled_pixels = np.count_nonzero(np.invert(disable_mask[start_column:stop_column, start_row:stop_row]))
            self.log.success('Found and disabled {0} noisy pixels.'.format(n_disabled_pixels))
        else:
            new_disabled_pixels_map = np.logical_and(np.logical_xor(self.data.original_disable_mask, disable_mask), self.data.original_disable_mask)
            n_disabled_pixels = np.count_nonzero(new_disabled_pixels_map[start_column:stop_column, start_row:stop_row])
            self.log.success('Found and disabled {0} new noisy pixels.'.format(n_disabled_pixels))

        # Set noise occupancy of noisy/disabled pixels to invalid value since not precisely measured (because have been disabled)
        self.data.noise_occupancy_map[self.data.noisy_map == 'n'] = -1

        self.h5_file.create_carray(self.h5_file.root, name='HistOcc', title='Occupancy', obj=self.data.occupancy,
                                   filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        self.h5_file.create_carray(self.h5_file.root, name='NoiseOccMap', title='Noise Occupancy Map', obj=self.data.noise_occupancy_map,
                                   filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        self.h5_file.create_carray(self.h5_file.root, name='DisabledPixels', title='Disabled Pixels', obj=disable_mask,
                                   filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        if reset_disable_mask:
            self.h5_file.create_carray(self.h5_file.root, name='DisabledPixelsAtStart', title='Disabled Pixels At Start', obj=self.data.reset_disable_mask,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        else:
            self.h5_file.create_carray(self.h5_file.root, name='DisabledPixelsAtStart', title='Disabled Pixels At Start', obj=self.data.original_disable_mask,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        self.log.success('Scan finished')

    def _analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            occupancy = in_file.root.HistOcc[:]
            final_disable_mask = in_file.root.DisabledPixels[:]
            initial_disable_mask = in_file.root.DisabledPixelsAtStart[:]

            # Get scan parameters
            scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])

            start_column = scan_config['start_column']
            stop_column = scan_config['stop_column']
            start_row = scan_config['start_row']
            stop_row = scan_config['stop_row']

            n_pixels = (stop_column - start_column) * (stop_row - start_row)

            n_disabled_pixels = np.count_nonzero(np.invert(final_disable_mask[start_column:stop_column, start_row:stop_row]))

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
                p.create_parameter_page()

                p._plot_occupancy(hist=occupancy.T, title='Occupancy', z_max=np.max(occupancy), suffix='occupancy')

                p._plot_occupancy(hist=np.ma.masked_array(np.invert(final_disable_mask), np.invert(initial_disable_mask)).T,
                                  electron_axis=False, z_label='Disabled', title='Disabled Pixel Map',
                                  use_electron_offset=False, show_sum=False,
                                  z_min=0, z_max=1,
                                  suffix='disabled_pixels_map')

        return n_disabled_pixels, round(float(n_disabled_pixels) / n_pixels * 100, 2), occupancy.sum(), final_disable_mask

    def analyze_data_online(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        self.data.hist_occ.add(raw_data)


if __name__ == '__main__':
    with NoiseOccAdvScan(scan_config=scan_configuration) as scan:
        scan.start()
