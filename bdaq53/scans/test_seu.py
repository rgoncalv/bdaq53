#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This test checks continuously for SEU induced bitflips
'''

import time
import tables as tb

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis.plotting import Plotting


scan_configuration = {
    'scan_timeout': False,
    'scan_interval': 10,
    'correct_errors': False
}


class DataTable(tb.IsDescription):
    timestamp = tb.Int64Col(pos=0)
    errors = tb.Int64Col(pos=1)
    integrated_errors = tb.Int64Col(pos=2)


class SEUTest(ScanBase):
    scan_id = 'seu_test'

    def _scan(self, scan_timeout=300, scan_interval=30, correct_errors=False, **_):
        '''
        SEU test main loop
        '''

        start_timestamp = time.time()
        stop_scan = False
        first_run = True
        delay = 0
        self.data.results = []

        while not stop_scan:
            try:
                time.sleep(scan_interval - delay)
                self.log.info('Reading registers...')
                self.data.results.append((time.time(), self.chip.registers.check_all(correct=correct_errors)))

                if scan_timeout and time.time() > (start_timestamp + scan_timeout):
                    stop_scan = True
                    self.log.info('Run timeout has been reached.')

                if not stop_scan:
                    self.log.info('Continuing scan...')

                if first_run:
                    delay = time.time() - start_timestamp - scan_interval
                    first_run = False

            except KeyboardInterrupt:
                stop_scan = True
                self.log.info('Scan was stopped due to keyboard interrupt')

        data_table = self.h5_file.create_table(self.h5_file.root, name='seu_data', title='SEU data', description=DataTable)
        for iteration in self.data.results:
            row = data_table.row
            row['timestamp'] = iteration[0]
            row['errors'] = iteration[1]
            row.append()

        self.log.success('Scan finished')

    def _analyze(self):
        au.copy_configuration_node(self.output_filename + '.h5', self.output_filename + '_interpreted.h5')
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            results = in_file.root.seu_data[:]
            with tb.open_file(self.output_filename + '_interpreted.h5', 'a') as out_file:
                in_file.copy_node(in_file.root.seu_data, out_file.root)

        timestamps, errors, integrated_errors = [], [], []

        for iteration in results:
            timestamps.append(iteration[0])
            errors.append(iteration[1])
            try:
                integrated_errors.append(integrated_errors[-1] + iteration[1])
            except IndexError:
                integrated_errors.append(0)

        if self.configuration['bench']['analysis']['create_pdf']:
            with Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5') as p:
                p.log.info('Creating selected plots...')
                p.create_parameter_page()
                p._plot_1d_vs_time([timestamps, errors], title='Register Errors', y_axis_title='# of errors')
                p._plot_1d_vs_time([timestamps, integrated_errors], title='Integrated Register Errors', y_axis_title='# of errors')


if __name__ == "__main__":
    with SEUTest(scan_config=scan_configuration) as test:
        test.start()
