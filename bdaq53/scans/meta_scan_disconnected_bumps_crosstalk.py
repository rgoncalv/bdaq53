#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Tune global and local threshold and scan for disconnected bump bonds using the crosstalk method.
    Also refer to scan_disconnected_bumps_crosstalk.py.

    Note:
    To obtain correct results the sensor should *not* be reverse-biased, but ideally biased
    at zero current (0A), which corresponds to a slight forward voltage around ~1e1 mV.
'''

from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.scan_disconnected_bumps_crosstalk import BumpConnCTalkScan


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    'sensor_pixel': 'square',   # 'square' (50x50) or 'rect' (25x100) pixels?

    'n_injections': 200,
    'max_noise_occ': 1e-4       # Target maximum noise occupancy from NoiseOccAdvScan
}

tuning_configuration = {
    'maskfile': None,

    'VCAL_MED': 500
}


if __name__ == '__main__':
    start_column = scan_configuration['start_column']
    stop_column = scan_configuration['stop_column']

    tuning_configuration['start_row'] = scan_configuration['start_row']
    tuning_configuration['stop_row'] = scan_configuration['stop_row']

    flavors = []
    if start_column < 128:
        flavors.append('SYNC')
    if start_column < 264 and stop_column > 128:
        flavors.append('LIN')
    if stop_column > 264:
        flavors.append('DIFF')

    # Tune global thresholds
    for flavor in flavors:
        if flavor == 'SYNC':
            tuning_configuration['start_column'] = 0
            tuning_configuration['stop_column'] = 128
            tuning_configuration['VCAL_HIGH'] = tuning_configuration['VCAL_MED'] + 225
        elif flavor == 'LIN':
            tuning_configuration['start_column'] = 128
            tuning_configuration['stop_column'] = 264
            tuning_configuration['VCAL_HIGH'] = tuning_configuration['VCAL_MED'] + 177
        elif flavor == 'DIFF':
            tuning_configuration['start_column'] = 264
            tuning_configuration['stop_column'] = 400
            tuning_configuration['VCAL_HIGH'] = tuning_configuration['VCAL_MED'] + 129

        with GDACTuning(scan_config=tuning_configuration) as global_tuning:
            global_tuning.start()

    # Tune local thresholds
    for flavor in flavors:
        if flavor == 'SYNC':  # No TDAC tuning for SYNC
            continue
        elif flavor == 'LIN':
            tuning_configuration['start_column'] = 128
            tuning_configuration['stop_column'] = 264
            tuning_configuration['VCAL_HIGH'] = tuning_configuration['VCAL_MED'] + 177
        elif flavor == 'DIFF':
            tuning_configuration['start_column'] = 264
            tuning_configuration['stop_column'] = 400
            tuning_configuration['VCAL_HIGH'] = tuning_configuration['VCAL_MED'] + 129
        with TDACTuning(scan_config=tuning_configuration) as local_tuning:
            local_tuning.start()

    with BumpConnCTalkScan(scan_config=scan_configuration) as disconn_bumps_scan:
        disconn_bumps_scan.start()
